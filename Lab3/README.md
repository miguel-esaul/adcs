# TC4002.1 | ADCS Lab 3. #

This project was created as an assignment for the postgraduate-level course *TC4002.1 Software Analysis, Design and 
Construction* taught at Tecnológico de Monterrey Campus Guadalajara by Ph.D. Gerardo Padilla Zárate.

### Prerequisites ###

* Python 3.6 or above
* `Requirements.txt` file list all Python libraries this project depends on, install them using:

```
pip install -r requirements.txt
```

### Notes ###

* If using an IDE like Pycharm, it is recommended to create a Run Configuration using the `Lab3_main.py` as script instead of the default Python test that Pycharm creates when opening the project.

* Coverage results are available under /htmlcov/

* * *

### Description ###

Original instructions and requirements for this Lab can be found in 
[Instructions](https://bitbucket.org/miguel-esaul/adcs/src/master/Lab3/Lab%20Instructions/).

#### Goal ####

* The target of these exercises was to enable the student to practice and master programming skills during the course.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

#### Requirements ####

* This program must implement unit test cases.
* Unit test cases must provide over 90% coverage.
* Python's built-in statistics method can not be used

#### Programming Exercises ####

The following section provides a brief description of the exercises required for this practice.

##### Exercise 16 #####
Given the `math.ceil` function,
* Define a set of unittest cases that exercise the function (Remember Right BICEP)

##### Exercise 17 #####
Given the `math.factorial` function
* Define a set of test cases that exercise the function (Remember Right BICEP)

##### Exercise 18 #####
Given the `math.pow` function
* Define a set of test cases that exercise the function (Remember Right BICEP)

##### Exercise 19 #####
Given the `filecmp.cmp` function
* Define a set of test cases that exercise the function (Remember Right BICEP)

##### Exercise 20 #####
Implement a class that manages a directory that is saved in a text file. The data saved includes: Name, Email, Age, 
Country of Origin. The class should have capabilities to:
1. Add new record
1. Delete a record
1. Look for a record by mail and age
1. List on screen all records

### Results ###

* 73 unit test cases were performed.
    * 6 Failures. Failures were not corrected for educational demonstration purposes.
* 100% statement coverage.

* * *

### Authors ###

* **Miguel Avina** - *Initial Work* - [Bitbucket](https://bitbucket.org/miguel-esaul), 
[Github](https://github.com/miguelAvina1).

### License ###

The MIT License (MIT)

Copyright (c) 2020 Miguel Aviña Pantoja

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
