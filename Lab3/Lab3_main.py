import filecmp
import io
import math
import os
import sys
import time
import unittest
from shutil import copyfile

from ddt import ddt, data, file_data, unpack

import directory as directory


# Programming exercise 16. Given the math.ceil function ################################################################


class TestingCeil(unittest.TestCase):
    def test_positive(self):                # RIGHT: Right
        self.assertEqual(10, math.ceil(9.242), 'Incorrect answer for positive float input')

    def test_large_positive(self):          # RIGHT: Boundary
        self.assertEqual(10, math.ceil(9.0000000000000001), 'Incorrect answer for large positive float input')

    def test_negative(self):                # RIGHT: Right
        self.assertEqual(-11, math.ceil(-11.57596))

    def test_large_negative(self):          # RIGHT: Boundary
        self.assertEqual(-10000000, math.ceil(-10000000.000000000000000001))

    def test_zero(self):                    # RIGHT: Boundary
        self.assertEqual(0, math.ceil(-0.0001))

    def test_inverse(self):                 # RIGHT: Inverse
        test_input = 23.2454
        ceil = math.ceil(test_input)
        floor = math.floor(test_input)
        self.assertEqual(1, ceil-floor)

    def test_cross_check(self):             # RIGHT: Cross-check
        test_input = 55422.433496901
        ceil = int(test_input) + 1
        self.assertEqual(ceil, math.ceil(test_input))

    def test_non_numeric(self):                             # RIGHT: Error condition
        self.assertRaises(TypeError, math.ceil, 'H')

    def test_null(self):                                    # RIGHT: Error condition
        self.assertRaises(TypeError, math.ceil, )

    def test_execution_time_small(self):                    # RIGHT: Performance
        start_time = time.time()
        math.ceil(104.48)
        execution_time = time.time() - start_time
        self.assertLess(execution_time, 0.00001)

    def test_execution_time_large(self):                    # RIGHT: Performance
        start_time = time.time()
        math.ceil(15687816123.4848612384385154681201)
        execution_time = time.time() - start_time
        self.assertLess(execution_time, 0.00001)


# Programming exercise 17. Given the math.factorial function ###########################################################


class TestingFactorial(unittest.TestCase):
    def test_single_positive(self):                         # RIGHT: Right
        self.assertEqual(479001600, math.factorial(12), 'Incorrect answer for positive input')

    def test_zero(self):                                    # RIGHT: Boundary
        self.assertEqual(1, math.factorial(0), 'Incorrect answer for zero input')

    def test_inverse(self):                                 # RIGHT: Inverse relation
        test_number = 3
        fact = math.factorial(test_number)
        for divisor in range(1, test_number+1):
            fact = fact/divisor
        self.assertEqual(1, fact)

    def test_cross_check(self):                             # RIGHT: Cross-check
        test_number = 9
        fact = 1
        for num in range(1, test_number+1):
            fact *= num
        self.assertEqual(fact, math.factorial(test_number))

    def test_decimal(self):                                 # RIGHT: Error condition
        self.assertRaises(ValueError, math.factorial, 0.1)

    def test_negative(self):                                # RIGHT: Error condition
        self.assertRaises(ValueError, math.factorial, -1)

    def test_non_numeric(self):                             # RIGHT: Error condition
        self.assertRaises(TypeError, math.factorial, 'a')

    def test_null(self):                                    # RIGHT: Error condition
        self.assertRaises(TypeError, math.factorial, )

    def test_execution_time_small(self):                    # RIGHT: Performance
        start_time = time.time()
        math.factorial(10)
        execution_time = time.time() - start_time
        self.assertLess(execution_time, 0.001)

    def test_execution_time_large(self):                    # RIGHT: Performance
        start_time = time.time()
        math.factorial(10000)
        execution_time = time.time() - start_time
        self.assertLess(execution_time, 0.01)


# Programming exercise 18. Given the math.pow function #################################################################


class TestingPow(unittest.TestCase):
    def test_positive_simple(self):                                # RIGHT: Right
        self.assertEqual(81, math.pow(9, 2), 'Incorrect answer for positive input')

    def test_positive_large(self):                                  # RIGHT: Right
        self.assertEqual(10460353203, math.pow(3, 21))

    def test_decimal_base(self):                                    # RIGHT: Right
        self.assertEqual(0.032186906916727206510367651443384001, math.pow(0.751, 12))

    def test_decimal_exponent(self):                                # RIGHT: Right
        self.assertEqual(440851666.507407983952833011475789515245937757207197323, math.pow(14, 7.54217))

    def test_decimal_base_and_exponent(self):                       # RIGHT: Right
        self.assertEqual(5047.908480532039950798025, math.pow(8.0001741, 4.1004471), 'Math,pow() failed with decimal '
                                                                                     'inputs')

    def test_zero_base(self):                                       # RIGHT: Boundary
        self.assertEqual(0, math.pow(0, 1431.2), 'Incorrect answer for zero base')

    def test_zero_exponent(self):                                   # RIGHT: Boundary
        self.assertEqual(1, math.pow(3245.225432, 0), 'Incorrect answer for zero exponent')

    def test_zero_base_and_exponent(self):                          # RIGHT: Boundary
        self.assertEqual(1, math.pow(0, 0))                         # Mathematical expression with no agreed-upon value.

    def test_inverse(self):                                         # RIGHT: Inverse relation
        test_number = 3.15851
        power = math.pow(test_number, 2)
        self.assertEqual(math.sqrt(power), test_number)

    def test_cross_check(self):                                     # RIGHT: Cross-check
        test_number = 5.23145
        exponent = 8
        result = 1
        for _ in range(exponent):
            result *= test_number
        self.assertEqual(result, math.pow(test_number, exponent), 'Math.pow() Failed with cross check')

    def test_non_numeric(self):                                     # RIGHT: Error condition
        self.assertRaises(TypeError, math.pow, 'a', 'b')

    def test_arguments_missing(self):                               # RIGHT: Error condition
        self.assertRaises(TypeError, math.pow, 2)

    def test_null(self):                                            # RIGHT: Error condition
        self.assertRaises(TypeError, math.pow, )

    def test_execution_time_small(self):                            # RIGHT: Performance
        start_time = time.time()
        math.pow(23, 3)
        execution_time = time.time() - start_time
        self.assertLess(execution_time, 0.0001)

    def test_execution_time_large(self):                            # RIGHT: Performance
        start_time = time.time()
        math.pow(5.0741041, 14.1748152102)
        execution_time = time.time() - start_time
        self.assertLess(execution_time, 0.0001)


# Programming exercise 19. Given the math.cmp function #################################################################


class TestingFileCompare(unittest.TestCase):
    def test_same_contents(self):                           # RIGHT: Right
        self.assertEqual(True, filecmp.cmp('Lab_Resources/L3_File1.txt', 'Lab_Resources/L3_File2.txt'))

    def test_same_contents_shallow_False(self):             # RIGHT: Right
        self.assertEqual(True, filecmp.cmp('Lab_Resources/L3_File1.txt', 'Lab_Resources/L3_File2.txt', False))

    def test_different_lineEnding(self):                    # RIGHT: Boundary
        self.assertEqual(False, filecmp.cmp('Lab_Resources/L3_CRLF.txt', 'Lab_Resources/L3_LF.txt'))

    def test_different_lineEnding_shallow_False(self):      # RIGHT: Boundary
        self.assertEqual(False, filecmp.cmp('Lab_Resources/L3_CRLF.txt', 'Lab_Resources/L3_LF.txt', False))

    def test_extra_empty_line(self):                        # RIGHT: Boundary
        self.assertEqual(False, filecmp.cmp('Lab_Resources/L3_File1.txt', 'Lab_Resources/L3_File1_extra.txt'))

    def test_extra_empty_line_shallow_False(self):          # RIGHT: Boundary
        self.assertEqual(False, filecmp.cmp('Lab_Resources/L3_File1.txt', 'Lab_Resources/L3_File1_extra.txt', False))

    def test_same_file(self):                               # RIGHT: Boundary
        self.assertEqual(True, filecmp.cmp('Lab_Resources/L3_File2.txt', 'Lab_Resources/L3_File2.txt'))

    def test_same_file_shallow_False(self):                 # RIGHT: Boundary
        self.assertEqual(True, filecmp.cmp('Lab_Resources/L3_File2.txt', 'Lab_Resources/L3_File2.txt', False))

    def test_cross_check(self):                             # RIGHT: Cross-check
        copyfile('Lab_Resources/L3_File1.txt', 'Lab_Resources/L3_File1_copy.txt')
        self.assertEqual(True, filecmp.cmp('Lab_Resources/L3_File1_copy.txt', 'Lab_Resources/L3_File1.txt'))

    def test_null(self):                                    # RIGHT: Error condition
        self.assertRaises(TypeError, filecmp.cmp, 'Lab_Resources/L3_File1.txt', )

    def test_numerical(self):                               # RIGHT: Error condition
        self.assertRaises(OSError, filecmp.cmp, 123, 0)

    def test_execution_time_small(self):                    # RIGHT: Performance
        start_time = time.time()
        filecmp.clear_cache()
        self.assertEqual(False, filecmp.cmp('Lab_Resources/L3_LargeTXT1.txt', 'Lab_Resources/L3_LargeTXT2.txt'))
        execution_time = time.time() - start_time
        self.assertLess(execution_time, 0.0001)

    def test_execution_time_small_shallow_false(self):      # RIGHT: Performance
        start_time = time.time()
        filecmp.clear_cache()
        self.assertEqual(False, filecmp.cmp('Lab_Resources/L3_LargeTXT1.txt', 'Lab_Resources/L3_LargeTXT2.txt'))
        execution_time = time.time() - start_time
        self.assertLess(execution_time, 0.0001)


# Programming exercise 20. Implement and test a class that manages a directory #########################################
# Below, a mix of possible ways to tests the directory are shown.
@ddt       # Decorators modify and extend the behaviour of a function/class. ddt is a callable function
class TestingDirectory(unittest.TestCase):
    @data(('Miguel', 'A01227898@itesm.mx', 24, 'Mexico'), ('Esaul', 'miguelavina16@gmail.com', 25, 'Espana'))
    @unpack
    def test_new_record_ddt_unpack(self, name, mail, age, country):
        dir1 = directory.UsersDirectory()       # Just testing for any error while using the method
        dir1.add_record(name, mail, age, country)
        self.assertEqual(dir1.directory, [[name, mail, age, country]])  # Testing if the method is saving the data

    @file_data("Lab_Resources/test_directory_ddt.json")
    def test_new_record_ddt_json(self, name, mail, age, country):
        dir1 = directory.UsersDirectory()
        dir1.add_record(name, mail, age, country)
        self.assertEqual(dir1.directory, [[name, mail, age, country]])  # Testing if the method is saving the data

    @data(('Miguel', 'A01227898@itesm.mx', 24, 'Mexico'))
    @unpack
    def test_delete_record_data(self, name, mail, age, country):
        dir1 = directory.UsersDirectory()
        dir1.add_record(name, mail, age, country)
        dir1.delete_record(0)
        self.assertEqual(len(dir1.directory), 0)

    def test_delete_not_existing(self):     # Just testing for any error while using the method
        captured_output = io.StringIO()     # Create StringIO object, this functions DOES NOT return, just prints
        sys.stdout = captured_output        # and redirect stdout.
        dir1 = directory.UsersDirectory()
        dir1.delete_record(2)
        sys.stdout = sys.__stdout__         # Reset redirect.
        self.assertEqual(captured_output.getvalue(), 'Given record does not exists\n')

    def test_look_by_mail(self):                # Just testing for any error while using the method
        dir1 = directory.UsersDirectory()
        dir1.add_record('Miguel', 'A01227898@itesm.mx', 24, 'Mexico')
        dir1.add_record('Esaul', 'miguelavina16@gmail.com', 25, 'Espana')
        dir1.add_record('Avina', 'miguel.avina@avnet.com', 27, 'Canada')
        self.assertEqual(['Esaul', 'miguelavina16@gmail.com', 25, 'Espana'], dir1.search('miguelavina16@gmail.com'))

    def test_look_by_age(self):                 # Just testing for any error while using the method
        dir1 = directory.UsersDirectory()
        dir1.add_record('Miguel', 'A01227898@itesm.mx', 24, 'Mexico')
        dir1.add_record('Esaul', 'miguelavina16@gmail.com', 25, 'Espana')
        dir1.add_record('Avina', 'miguel.avina@avnet.com', 27, 'Canada')
        self.assertEqual(['Avina', 'miguel.avina@avnet.com', 27, 'Canada'], dir1.search(27))

    def test_look_not_existing(self):
        dir1 = directory.UsersDirectory()
        dir1.add_record('Miguel', 'A01227898@itesm.mx', 24, 'Mexico')
        dir1.add_record('Esaul', 'miguelavina16@gmail.com', 25, 'Espana')
        dir1.add_record('Avina', 'miguel.avina@avnet.com', 27, 'Canada')  # This function DOES returns, so IO wrapper
        self.assertEqual('Not found in directory', dir1.search(100))      # not needed

    def test_get_record(self):
        dir1 = directory.UsersDirectory()
        dir1.add_record('Miguel', 'A01227898@itesm.mx', 24, 'Mexico')
        dir1.add_record('Esaul', 'miguelavina16@gmail.com', 25, 'Espana')
        dir1.add_record('Avina', 'miguel.avina@avnet.com', 27, 'Canada')
        self.assertEqual(['Esaul', 'miguelavina16@gmail.com', 25, 'Espana'], dir1.get_record(1))

    def test_creating_file_formatted(self):       # Checking if the method creates the specified file
        dir1 = directory.UsersDirectory()
        dir1.add_record('Miguel', 'A01227898@itesm.mx', 24, 'Mexico')
        dir1.add_record('Esaul', 'miguelavina16@gmail.com', 25, 'Espana')
        dir1.save_to_file_formatted('Lab_Resources/f_testing_file.txt')
        self.assertEqual(True, os.path.isfile('Lab_Resources/f_testing_file.txt'))
        os.remove('Lab_Resources/f_testing_file.txt')     # Erasing the file

    def test_checking_file_contents_formatted(self):       # Checking if the method creates the specified file
        dir1 = directory.UsersDirectory()
        dir1.add_record('Miguel', 'A01227898@itesm.mx', 24, 'Mexico')
        dir1.add_record('Esaul', 'miguelavina16@gmail.com', 25, 'Espana')
        dir1.save_to_file_formatted('Lab_Resources/f_testing_file.txt')
        file = open('Lab_Resources/f_testing_file.txt')
        contents = file.readlines()
        file.close()
        file = open('Lab_Resources/f_testing_file_expected.txt')
        expected_contents = file.readlines()
        file.close()
        self.assertEqual(contents, expected_contents)
        os.remove('Lab_Resources/f_testing_file.txt')     # Erasing the file

    def test_creating_file(self):       # Checking if the method creates the specified file
        dir1 = directory.UsersDirectory()
        dir1.add_record('Miguel', 'A01227898@itesm.mx', 24, 'Mexico')
        dir1.add_record('Esaul', 'miguelavina16@gmail.com', 25, 'Espana')
        dir1.save_to_file('Lab_Resources/uf_testing_file.txt')
        self.assertEqual(True, os.path.isfile('Lab_Resources/uf_testing_file.txt'))
        os.remove('Lab_Resources/uf_testing_file.txt')     # Erasing the file

    def test_checking_file_contents(self):       # Checking if the method creates the specified file
        dir1 = directory.UsersDirectory()
        dir1.add_record('Miguel', 'A01227898@itesm.mx', 24, 'Mexico')
        dir1.add_record('Esaul', 'miguelavina16@gmail.com', 25, 'Espana')
        dir1.save_to_file('Lab_Resources/uf_testing_file.txt')
        file = open('Lab_Resources/uf_testing_file.txt')
        contents = file.readlines()
        file.close()
        file = open('Lab_Resources/uf_testing_file_expected.txt')
        expected_contents = file.readlines()
        file.close()
        self.assertEqual(contents, expected_contents)
        os.remove('Lab_Resources/uf_testing_file.txt')     # Erasing the file

    def test_load_file_formatted(self):
        dir1 = directory.UsersDirectory()
        dir1.load_from_formatted_file('Lab_Resources/f_testing_file_expected.txt')
        expected_data = [['Miguel', 'A01227898@itesm.mx', '24', 'Mexico'],
                         ['Esaul', 'miguelavina16@gmail.com', '25', 'Espana']]
        self.assertEqual(dir1.directory, expected_data)

    def test_load_file(self):
        dir1 = directory.UsersDirectory()
        dir1.load_from_file('Lab_Resources/uf_testing_file_expected.txt')
        expected_data = [['Miguel', 'A01227898@itesm.mx', '24', 'Mexico'],
                         ['Esaul', 'miguelavina16@gmail.com', '25', 'Espana']]
        self.assertEqual(dir1.directory, expected_data)

    @file_data("Lab_Resources/test_directory_ddt.json")
    def test_print_records(self, name, mail, age, country):
        dir1 = directory.UsersDirectory()
        dir1.add_record(name, mail, age, country)
        captured_output = io.StringIO()
        sys.stdout = captured_output
        dir1.print_directory()
        sys.stdout = sys.__stdout__
        expected_output = "Current directory:\n['{}', '{}', {}, '{}']\n".format(name, mail, age, country)
        self.assertEqual(captured_output.getvalue(), expected_output)


if __name__ == '__main__':
    unittest.main()
