var NAVTREE =
[
  [ "Sorting algorithm.", "index.html", [
    [ "Goal", "index.html#Goal", null ],
    [ "Requirements", "index.html#requirements", null ],
    [ "Programming Exercises", "index.html#exercises", [
      [ "Exercise 25", "index.html#Exercise_25", null ],
      [ "Exercise 26", "index.html#Exercise_26", null ],
      [ "Exercise 27", "index.html#Exercise_27", null ]
    ] ],
    [ "Results", "index.html#results", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Packages", null, [
      [ "Packages", "namespaces.html", "namespaces" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"CustomExceptions_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';