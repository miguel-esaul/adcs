var modules =
[
    [ "Custom Exceptions", "group__exceptions.html", "group__exceptions" ],
    [ "Testing Framework", "group__testing.html", "group__testing" ],
    [ "Main", "group__main.html", "group__main" ],
    [ "Sort", "group__merge__sort.html", "group__merge__sort" ],
    [ "Utils", "group__utils.html", "group__utils" ]
];