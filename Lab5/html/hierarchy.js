var hierarchy =
[
    [ "Exception", null, [
      [ "CustomExceptions.Error", "classCustomExceptions_1_1Error.html", [
        [ "CustomExceptions.CVSFileNotFound", "classCustomExceptions_1_1CVSFileNotFound.html", null ],
        [ "CustomExceptions.EmptyFile", "classCustomExceptions_1_1EmptyFile.html", null ],
        [ "CustomExceptions.IncorrectType", "classCustomExceptions_1_1IncorrectType.html", null ],
        [ "CustomExceptions.InputFileNotSpecified", "classCustomExceptions_1_1InputFileNotSpecified.html", null ],
        [ "CustomExceptions.InvalidFile", "classCustomExceptions_1_1InvalidFile.html", null ]
      ] ]
    ] ],
    [ "Lab5_sortingClass.SortingCSVData", "classLab5__sortingClass_1_1SortingCSVData.html", null ],
    [ "TestCase", null, [
      [ "Lab5_main.TestingSortingClass", "classLab5__main_1_1TestingSortingClass.html", null ],
      [ "Lab5_main.TestingUtilsFunctions", "classLab5__main_1_1TestingUtilsFunctions.html", null ]
    ] ]
];