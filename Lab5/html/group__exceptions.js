var group__exceptions =
[
    [ "Error", "classCustomExceptions_1_1Error.html", null ],
    [ "CVSFileNotFound", "classCustomExceptions_1_1CVSFileNotFound.html", null ],
    [ "IncorrectType", "classCustomExceptions_1_1IncorrectType.html", null ],
    [ "InputFileNotSpecified", "classCustomExceptions_1_1InputFileNotSpecified.html", null ],
    [ "InvalidFile", "classCustomExceptions_1_1InvalidFile.html", null ],
    [ "EmptyFile", "classCustomExceptions_1_1EmptyFile.html", null ]
];