var group__testing =
[
    [ "TestingUtilsFunctions", "classLab5__main_1_1TestingUtilsFunctions.html", [
      [ "test_generate_file", "classLab5__main_1_1TestingUtilsFunctions.html#acea40ff647dae2e4d29a9745b70348c7", null ],
      [ "test_generate_file_bad_input", "classLab5__main_1_1TestingUtilsFunctions.html#aedfd8f6fc617f920f580fb1ea1b31284", null ],
      [ "test_generate_file_missing_arg", "classLab5__main_1_1TestingUtilsFunctions.html#a403abfe2503695915d1c822d2f0f96c8", null ]
    ] ],
    [ "TestingSortingClass", "classLab5__main_1_1TestingSortingClass.html", [
      [ "test_built_in_type_error_output", "classLab5__main_1_1TestingSortingClass.html#a02e4f510535ab2f653db41ef23f712f5", null ],
      [ "test_generic_error_output", "classLab5__main_1_1TestingSortingClass.html#a2be5526c93f38ff2d1f5fc9870cb0739", null ],
      [ "test_invalid_input_file", "classLab5__main_1_1TestingSortingClass.html#a5a60cc3aa7321189432933b7c0d47b9c", null ],
      [ "test_invalid_output_file", "classLab5__main_1_1TestingSortingClass.html#a8d01eeffe96454a02fc2772d5e6e5835", null ],
      [ "test_no_permission_output", "classLab5__main_1_1TestingSortingClass.html#a7bdbf1e7644df2a921b7b60c05fe8287", null ],
      [ "test_not_existing_input", "classLab5__main_1_1TestingSortingClass.html#ab0ee782b34c049451cd50d1819e6e0ae", null ],
      [ "test_not_input_specified", "classLab5__main_1_1TestingSortingClass.html#adb8c8dfda49c8fac808758a4fb05d770", null ],
      [ "test_performance_float", "classLab5__main_1_1TestingSortingClass.html#a63e4c887b47076d7e2fa6c9ee93efa54", null ],
      [ "test_performance_int", "classLab5__main_1_1TestingSortingClass.html#a8aff6fa3b2d1cd5120e75c13a7bd13ee", null ],
      [ "test_set_output", "classLab5__main_1_1TestingSortingClass.html#aaf0c3d872e9a05bfb020965d346f512f", null ],
      [ "test_sort_100k_float_right", "classLab5__main_1_1TestingSortingClass.html#af071ccea5161ff1c62f6df14a7dd3655", null ],
      [ "test_sort_100k_values_right", "classLab5__main_1_1TestingSortingClass.html#ad297e55ed580ad755948d2c32d1297e6", null ],
      [ "test_sort_corner_case", "classLab5__main_1_1TestingSortingClass.html#a3104df81b1d29b8ea3972b80376acbc7", null ],
      [ "test_sort_less_10_values_right", "classLab5__main_1_1TestingSortingClass.html#aa429d5fd8cb829fe586a8ce50ecb42a7", null ],
      [ "test_sort_massive_files_float_right", "classLab5__main_1_1TestingSortingClass.html#aa9344ba83d150c7224a4d4af902548c7", null ],
      [ "test_sort_massive_files_right", "classLab5__main_1_1TestingSortingClass.html#a58fcbda8884af57a6d8e2c0db46c58bc", null ],
      [ "test_sort_no_values", "classLab5__main_1_1TestingSortingClass.html#ae72a27d6729cb1c9b2c902e777d529c5", null ]
    ] ]
];