# TC4002.1 | ADCS Lab 5. #

This project was created as an assignment for the postgraduate-level course *TC4002.1 Software Analysis, Design and 
Construction* taught at Tecnológico de Monterrey Campus Guadalajara by Ph.D. Gerardo Padilla Zárate.

### Prerequisites ###

* Python 3.6 or above
* `Requirements.txt` file list all Python libraries this project depends on, install them using:

```
pip install -r requirements.txt
```

### Notes ###

* If using an IDE like Pycharm, it is recommended to create a Run Configuration using the `Lab5_main.py` as script instead of the default Python test that Pycharm creates when opening the project.

* Documentation for this lab can be find under /html/index.html. Also a compiled version is provided: `Lab5 Help.chm`.

* Coverage results are available under /htmlcov/

* There are several commented sections in the code because the execution of `merge_sort()` with more than 1 million data 
takes to long to finish. Files needed to for test cases of over 1 million data are not included in this repo for size 
limitations. However the user can create them using:

    `generate_csv_file(filename, rows, columns, decimals, lower_l, upper_l)`

* * *

### Description ###

Original instructions and requirements for this Lab can be found in 
[Instructions](https://bitbucket.org/miguel-esaul/adcs/src/master/Lab5/Lab%20Instructions/).

#### Goal ####

* The target of these exercises was to enable the student to practice and master programming skills during the course.

#### Requirements ####

* This program must implement unit test cases.
* Unit test cases must provide over 90% coverage.
* Python's built-in sort methods can not be used

#### Programming Exercises ####

The following section provides a brief description of the exercises required for this practice.

##### Exercise 25 #####

Create a class in python that Implements a method to sort large amount of data in lists (thousands and millions of items). 
The data comes into CSV files.

Implement the method:

* `set_input_data(file_path_name)` 

This methods sets the information about the file that will be used to read the dataDefine custom exceptions or error
 codes for situations where the parameter is incorrect or the file can not be read

##### Exercise 26 #####

Implement the method:

* `set_output_data(file_path_name)` 

This methods sets the information about the file that will be used to store the sorted dataDefine custom exceptions or 
error codes for situations where the parameter is incorrect or the file can not be created

##### Exercise 27 #####

Implement the method:

* `execute_merge_sort()` 

This methods sorts the data contained in the file specified. Define custom exceptions or error codes for situations 
where there may be special errors

### Results ###

* 34 unit test cases were performed.
* 100% statement coverage.

* * *

### Authors ###

* **Miguel Avina** - *Initial Work* - [Bitbucket](https://bitbucket.org/miguel-esaul), 
[Github](https://github.com/miguelAvina1).

### License ###

The MIT License (MIT)

Copyright (c) 2020 Miguel Aviña Pantoja

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
