from random import uniform
import CustomExceptions as Ex
import csv

## @defgroup utils Utils
#  @brief This module groups all helper functions needed in the rest of the modules.
#


## @ingroup utils
def generate_csv_file(filename, rows, columns, decimals, lower_l, upper_l):
    ##
    # @brief Generates a CSV file with rows*columns values.
    #
    # The decimals of the output values can be specified as an integer.  A lower limit lower_l and an upper limit
    # upper_l must be specified and will be taken as the range for the generated random numbers.
    # @param filename name of the file with the comma-separated values
    # @param rows Number of rows in the output file
    # @param columns Number of columns in the output file
    # @param decimals Decimal positions for each value
    # @param lower_l Minimum possible value
    # @param upper_l Maximum possible value
    # @return None
    if type(filename) != str:
        raise Ex.IncorrectType  # Custom exception
    else:
        with open(filename, 'w', newline='') as file:
            writer = csv.writer(file)
            for _ in range(rows):        # Number of rows
                writer.writerow(['%.{}f'.format(decimals) % (uniform(lower_l, upper_l)) for _ in range(columns)])


## @ingroup utils
def python_built_in_sort(input_filename, output_filename):
    ##
    # @brief Sorts the values in the input file using the python Sort() method and save the results in the output file.
    #
    # This function is intended to be used as a reference point for the unit test of the custom sorting class.
    # @param input_filename name of the file with the comma-separated values
    # @param output_filename name of the file where the sorted values will be stored
    # @return None
    with open(input_filename, 'r') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        try:
            all_data = [int(j) for i in reader for j in i]
        except ValueError:
            all_data = [float(j) for i in reader for j in i]
        sorted_data = sorted(all_data)
        save_data_to_csv(sorted_data, output_filename)


## @ingroup utils
def save_data_to_csv(data, output_filename):
    ##
    # @brief Save an iterable object's data into a cvs file.
    #
    # This function divides the contents of the iterable objects in rows of 10 and stores them into the output file.
    # @param data iterable object with integer or float values
    # @param output_filename name of the file where the values will be stored
    # @return None
    with open(output_filename, 'w', newline='') as file:
        writer = csv.writer(file)
        if len(data) < 10:
            rows = 1
            columns = len(data)
        else:
            rows = len(data) // 10
            columns = 10
        i = 0
        for _ in range(rows):  # Number of rows
            writer.writerow(data[i:i + columns])
            i += 10
