import CustomExceptions as Ex
import csv
import Lab5_utils as Utils

## @defgroup merge_sort Sort
#  @brief This module groups all functions and classes for the sorting algorithm.
#


## @ingroup merge_sort
def merge_sorted(list1, list2):
    ##
    # This function merge the contents of two list in an sorted way.
    # The function compares the first element in the second list with the value pointed by an index <CODE>i</CODE> in
    # the first list; if the value of the second list is lesser than the value pointed in the first list, the function
    # pops the value of the second list and insert it in the index pointed by <CODE>i</CODE>, then i is
    # incremented by 1.
    # This process is repeated until the second list is empty or if all the remaining elements in the second list are
    # greater than the current elements in the first list, if the latter happens, the second list is appended to the end
    # of the first list.
    # @param list1 first list to merge
    # @param list2 second list to merge
    # @return An list with all elements sorted
    #
    i = 0
    while len(list2) > 0 and i < len(list1):
        if list2[0] < list1[i]:
            n = list2.pop(0)
            list1.insert(i, n)
        i += 1
    list1.extend(list2)
    return list1


## @ingroup merge_sort
def do_merge_sort(data):
    ##
    # This function implements the actual merge sort algorithm.
    # The function is a recursive function that split the elements in iterable object given as a parameter in half and
    # then calls itself providing the left half as the input iterable object. This process continue until the left half
    # of the data is just one element and the right half is one or two elements, after this, these two halves are taken
    # as parameters for the merge_sort function which merges its content in a sorted way. Then this function starts
    # going up in the recursive loop until finishing sorting the data.
    # @param data iterable object with the elements to sort
    # @return A list with all elements from the input data sorted.
    #
    if len(data) == 0:
        raise Ex.EmptyFile
    elif len(data) == 1:
        return data
    else:
        if len(data) % 2:
            half = len(data) // 2 + 1
        else:
            half = len(data) // 2

        left = data[:half]  # 0 to half non inclusive
        right = data[half:]

        left = do_merge_sort(left)
        right = do_merge_sort(right)

    return merge_sorted(left, right)


## @ingroup merge_sort
class SortingCSVData:
    ##
    # This class implements the methods needed to sort values from a cvs file..
    #
    def __init__(self):
        """ Initialization method. Set a default output file name in case the user doesn't provide one. """
        ## String that stores the name of input file with the comma-separated values.
        self.input_file_path = ''

        ## String that stores the name of the desired output file.
        self.output_file_path = 'Merge Sort Output.csv'

        ## Boolean variable that stores the status of the input path.
        self.correctInputPath = False

    def set_input_data(self, file_path_name):
        """
        This function sets the input file name to the object.
        It also asserts parameters and raise custom exceptions for situations where the parameter is incorrect.
        :param file_path_name: Input file name
        :return: None
        """
        if type(file_path_name) != str:
            raise Ex.IncorrectType     # Custom exception
        else:
            try:
                f = open(file_path_name, 'r')
                self.input_file_path = file_path_name
                self.correctInputPath = True
                f.close()
            except FileNotFoundError:
                raise Ex.CVSFileNotFound

    def set_output_data(self, file_path_name):
        """
        This function sets the output file name of the file that will stored the sorted values.
        It also asserts parameters and raise custom exceptions for situations where the parameter is incorrect.
        :param file_path_name: Output file name
        :return: None
        """
        if type(file_path_name) != str:
            raise Ex.IncorrectType  # Custom exception
        else:
            try:
                f = open(file_path_name, 'w')
                self.output_file_path = file_path_name
                f.close()
            except PermissionError:
                raise Ex.InvalidFile
            except FileNotFoundError:                       # Although built in exception is specific,
                print("There was an unexpected error")      # we raise a generic custom exception
                raise Ex.Error                              # for demonstration purposes.

    def execute_merge_sort(self):
        ##
        # This function retrieve the data from the input file name and put it into a list, then it calls
        # do_merge_sort function to sort the data. Finally, it stores the sorted data in the specified output file name.
        # @return None
        #
        if self.correctInputPath:
            with open(self.input_file_path, 'r') as csv_file:
                reader = csv.reader(csv_file, delimiter=',')
                try:
                    all_data = [int(j) for i in reader for j in i]
                except ValueError:
                    all_data = [float(j) for i in reader for j in i]
            sorted_data = do_merge_sort(all_data)
            Utils.save_data_to_csv(sorted_data, self.output_file_path)
        else:
            raise Ex.InputFileNotSpecified
