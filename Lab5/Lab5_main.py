import csv
import filecmp
import time

import Lab5_utils as Utils
import Lab5_sortingClass as Sort
import unittest
from ddt import data, unpack, ddt
import CustomExceptions as Ex
import os
from stat import S_IREAD, S_IRGRP, S_IROTH, S_IWUSR


##  @mainpage
#   @{
#   @section Goal Goal
#   The target of these exercises was to enable the student to practice and master programming skills during the course.
#
#
#   @section requirements Requirements
#   - This program must implement unit test cases.
#   - Unit test cases must provide over 90% coverage.
#   - Python's built-in sort method is prohibited.
#
#
#   @section exercises Programming Exercises
#   The specific exercises made in order to create a functioning sorting algorithm class are described below.
#
#   @subsection Exercise_25 Exercise 25
#   Create a class in python that Implements a method to sort large amount of data in lists
#   (thousands and millions of items).
#   The data comes into CVS files.
#   -# Implement the method:
#       - <CODE>set_input_data(file_path_name)</CODE><br>
#         This methods sets the information about the file that will be used to read the data.
#   -# Define <b>custom exceptions</b> or error codes for situations where the parameter is incorrect
#   or the file can not be read.
#
#   @subsection Exercise_26 Exercise 26
#   -# Implement the method:
#       - <CODE>set_output_data(file_path_name)</CODE><br>
#         This methods sets the information about the file that will be used to store the sorted data.
#   -# Define <b>custom exceptions</b> or error codes for situations where the parameter is incorrect
#   or the file can not be created.
#
#   @subsection Exercise_27 Exercise 27
#   -# Implement the method:
#       - <CODE>execute_merge_sort()</CODE><br>
#         This methods sorts the data contained in the file specified.
#   -# Define <b>custom exceptions</b> or error codes for situations where there may be special errors.
#
#
#   @section results Results
#   - 34 test cases were performed.
#   - 100% statement coverage.@ref footnote1 "1"
#
#   <br>
#   @note Two different types of comment blocks were used in the source code for demonstration purposes:
#   -# Standard <i>pythonic</i> way: Documentation strings (docstrings) used to describe classes, methods and functions.
#   In this case <b>none of doxygen's special commands are supported</b>. This is reflected in some member
#   functions documentation where the arguments and return value are not nicely formatted. See image below.
#   @image html unformatted_doc.png
#   -# Using C-like comments: Documentation comments starts with "##", more in line with they way documentation blocks
#   work for other language supported by doxygen, allowing the use of special commands. This is reflected in a nicely
#   formatted html output. See image below.
#   @image html formated_doc.png
#
#   <br><hr>
#   @author     Miguel Avi&ntilde;a
#   @date       March, 2020
#   @copyright  GNU Public License.
#
#   <br><hr>
#   @anchor footnote1 1. User must set <CODE>test_large_cases</CODE> to <CODE>True</CODE> to achieve this percentage.
#   @}
#

## @defgroup testing Testing Framework
#  @brief This module groups all classes and related functions regarding the unit tests for the whole project.
#
#  The unnittest built-in framework for python was used to test the sorting algorithm as well as the Lab5_utils.
#  Two different classes were used to test them. Also data driven test (DDT) was performed.

## @ingroup testing
@ddt
class TestingUtilsFunctions(unittest.TestCase):
    """ Tester class for the Lab5_utils module """
    @data('Lab_Resources/Example1.csv', 'Lab_Resources/Example2.csv')
    def test_generate_file(self, filename):
        """
        Test the generate_cvs_file function from the Utils module with expected parameter types.
        :param filename: string name of the file
        :return: None
        """
        Utils.generate_csv_file(filename, 10, 10, 2, 0, 100)
        self.assertEqual(True, os.path.isfile(filename))
        os.remove(filename)

    @data(82, 755, 1004.4)
    def test_generate_file_bad_input(self, filename):
        ##
        # Test the generate_cvs_file with incorrect data types input: int instead of string.
        # @param filename name of the file
        # @return None
        self.assertRaises(Ex.IncorrectType, Utils.generate_csv_file, filename, 10, 10, 2, 0, 100)

    @data(('F', 123, 54), (12, 545, 1), (1, 4, 'a'))
    @unpack
    def test_generate_file_missing_arg(self, arg1, arg2, arg3):
        ##
        # Test the generate_cvs_file with less arguments than expected.
        # @param arg1 Random first argument for generate_cvs_file function
        # @param arg2 Random second argument for generate_cvs_file function
        # @param arg3 Random third argument for generate_cvs_file function
        # @return None
        self.assertRaises(TypeError, Utils.generate_csv_file, arg1, arg2, arg3)


## @ingroup testing
@ddt
class TestingSortingClass(unittest.TestCase):
    """
    Tester class for the Lab5_sortingClass module.
    """
    def test_not_input_specified(self):
        """
        Test the case where no input file is specified and execute merge sort is called.
        :return: None
        """
        sort_object = Sort.SortingCSVData()
        self.assertRaises(Ex.InputFileNotSpecified, sort_object.execute_merge_sort)

    @data(82, 755, 1004.4)
    def test_invalid_input_file(self, filename):
        """
        Test set_input_data method from SortingCVSData class when incorrect input types are given.
        :return: None
        """
        sort_object = Sort.SortingCSVData()
        self.assertRaises(Ex.IncorrectType, sort_object.set_input_data, filename)

    @data('Lab_Resources/File1.csv', 'Lab_Resources/File2.csv', 'Lab_Resources/File3.csv')
    def test_not_existing_input(self, filename):
        """
        Test set_input_data method from SortingCVSData class when correct input types are given but files are missing
        in the current working directory.
        :param filename: name of the file
        :return: None
        """
        sort_object = Sort.SortingCSVData()
        self.assertRaises(Ex.CVSFileNotFound, sort_object.set_input_data, filename)

    @data('Lab_Resources/File1_output.csv', 'Lab_Resources/File2_output.csv', 'Lab_Resources/File3_output.csv')
    def test_set_output(self, filename):
        """
        Test set_output_data method from SortingCVSData class with correct and valid filenames.
        :param filename: name of the file
        :return: None
        """
        sort_object = Sort.SortingCSVData()
        sort_object.set_output_data(filename)
        self.assertEqual(sort_object.output_file_path, filename)

    def test_no_permission_output(self):
        """
        Test set_output_data method from SortingCVSData for a "read-only" file.
        :return: None
        """
        with open('Lab_Resources/read_only_file.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["SN", "Name", "Contribution"])
        filename = "Lab_Resources/read_only_file.csv"
        os.chmod(filename, S_IREAD | S_IRGRP | S_IROTH)
        sort_object = Sort.SortingCSVData()
        self.assertRaises(Ex.InvalidFile, sort_object.set_output_data, filename)
        os.chmod(filename, S_IWUSR | S_IREAD)  # This makes the file read/write for the owner
        os.remove("Lab_Resources/read_only_file.csv")

    @data(82, 755, 1004.4)
    def test_invalid_output_file(self, filename):
        """
        Test set_output_data method from SortingCVSData class when incorrect input types are given.
        :return: None
        """
        sort_object = Sort.SortingCSVData()
        self.assertRaises(Ex.IncorrectType, sort_object.set_output_data, filename)

    def test_built_in_type_error_output(self):
        """
        Test set_output_data method from SortingCVSData when no arguments are given and a Type error is expected.
        :return: None
        """
        sort_object = Sort.SortingCSVData()
        self.assertRaises(TypeError, sort_object.set_output_data, )

    def test_generic_error_output(self):
        """
        Test set_output_data method from SortingCVSData when no arguments are given and a custom error is expected.
        :return: None
        """
        sort_object = Sort.SortingCSVData()
        self.assertRaises(Ex.Error, sort_object.set_output_data, '')

    @data(('Lab_Resources/random_int_5.csv', 'Lab_Resources/sorted_int_5.csv',
           'Lab_Resources/sorted_int_5_expected.csv'), ('Lab_Resources/random_int_10.csv',
                                                        'Lab_Resources/sorted_int_10.csv',
                                                        'Lab_Resources/sorted_int_10_expected.csv'))
    @unpack
    def test_sort_less_10_values_right(self, input_filename, output_filename, expected_output_filename):
        ##
        # Test the execute_merge_sort method from the SoringCVSData with correct input and output file names
        # and less than 10 integer values input for sort. It validates the output using python's built-in sort method.
        # @param input_filename name of the file with the integer values to sort
        # @param output_filename name of the file that will be created with the sorted elements
        # @param expected_output_filename name of the file that will be created using python's built-in sort method that
        # will contain the correct sorted elements.
        # @return None
        sort_object = Sort.SortingCSVData()
        sort_object.set_input_data(input_filename)
        sort_object.set_output_data(output_filename)
        sort_object.execute_merge_sort()

        Utils.python_built_in_sort(input_filename, expected_output_filename)  # Reference to compare
        self.assertEqual(True, filecmp.cmp(output_filename, expected_output_filename))

    @data(('Lab_Resources/random_int_100k.csv', 'Lab_Resources/sorted_int_100k.csv',
           'Lab_Resources/sorted_int_100k_expected.csv'))
    @unpack
    def test_sort_100k_values_right(self, input_filename, output_filename, expected_output_filename):
        ##
        # Test the execute_merge_sort method from the SoringCVSData with correct input and output file names
        # and 100 thousand integer values input for sort. It validates the output using python's built-in sort method.
        # @param input_filename name of the file with the integer values to sort
        # @param output_filename name of the file that will be created with the sorted elements
        # @param expected_output_filename name of the file that will be created using python's built-in sort method that
        # will contain the correct sorted elements.
        # @return None
        sort_object = Sort.SortingCSVData()
        sort_object.set_input_data(input_filename)
        sort_object.set_output_data(output_filename)
        sort_object.execute_merge_sort()

        Utils.python_built_in_sort(input_filename, expected_output_filename)  # Reference to compare
        self.assertEqual(True, filecmp.cmp(output_filename, expected_output_filename))

    @data(('Lab_Resources/random_int_1M.csv', 'Lab_Resources/sorted_int_1M.csv',
           'Lab_Resources/sorted_int_1M_expected.csv')
          # ,('Lab_Resources/random_int_10M.csv', 'Lab_Resources/sorted_int_10M.csv',
          # 'Lab_Resources/sorted_int_10M_expected.csv'),
          # ('Lab_Resources/random_int_100M.csv', 'Lab_Resources/sorted_int_100M.csv',
          # 'Lab_Resources/sorted_int_100M_expected.csv')
          )
    @unpack
    def test_sort_massive_files_right(self, input_filename, output_filename, expected_output_filename):
        ##
        # Test the execute_merge_sort method from the SoringCVSData with correct input and output file names
        # and 1 million integer values input for sort. It validates the output using python's built-in sort method.
        # @param input_filename name of the file with the integer values to sort
        # @param output_filename name of the file that will be created with the sorted elements
        # @param expected_output_filename name of the file that will be created using python's built-in sort method that
        # will contain the correct sorted elements.
        # @return None
        # @note If test_large_cases boolean is set to false, this test case will skip the execution of method to be
        # tested (execute_merge_sort) and always return True.
        #
        if test_large_cases:
            sort_object = Sort.SortingCSVData()
            sort_object.set_input_data(input_filename)
            sort_object.set_output_data(output_filename)
            sort_object.execute_merge_sort()

            Utils.python_built_in_sort(input_filename, expected_output_filename)  # Reference to compare
            self.assertEqual(True, filecmp.cmp(output_filename, expected_output_filename))
        else:
            self.assertEqual(1, 1)  # Return always True.

    @data(('Lab_Resources/random_100k.csv', 'Lab_Resources/sorted_100k.csv',
           'Lab_Resources/sorted_100k_expected.csv'))
    @unpack
    def test_sort_100k_float_right(self, input_filename, output_filename, expected_output_filename):
        ##
        # Test the execute_merge_sort method from the SoringCVSData with correct input and output file names
        # and 100 thousand float values input for sort. It validates the output using python's built-in sort method.
        # @param input_filename name of the file with the integer values to sort
        # @param output_filename name of the file that will be created with the sorted elements
        # @param expected_output_filename name of the file that will be created using python's built-in sort method that
        # will contain the correct sorted elements.
        # @return None
        sort_object = Sort.SortingCSVData()
        sort_object.set_input_data(input_filename)
        sort_object.set_output_data(output_filename)
        sort_object.execute_merge_sort()

        Utils.python_built_in_sort(input_filename, expected_output_filename)  # Reference to compare
        self.assertEqual(True, filecmp.cmp(output_filename, expected_output_filename))

    @data(('Lab_Resources/random_1M.csv', 'Lab_Resources/sorted_1M.csv', 'Lab_Resources/sorted_1M_expected.csv')
          # ,('Lab_Resources/random_10M.csv', 'Lab_Resources/sorted_10M.csv', 'Lab_Resources/sorted_10M_expected.csv'),
          # ('Lab_Resources/random_100M.csv', 'Lab_Resources/sorted_100M.csv', 'Lab_Resources/sorted_100M_expected.csv')
          )
    @unpack
    def test_sort_massive_files_float_right(self, input_filename, output_filename, expected_output_filename):
        ##
        # Test the execute_merge_sort method from the SoringCVSData with correct input and output file names
        # and 1 million float values input for sort. It validates the output using python's built-in sort method.
        # @param input_filename name of the file with the integer values to sort
        # @param output_filename name of the file that will be created with the sorted elements
        # @param expected_output_filename name of the file that will be created using python's built-in sort method that
        # will contain the correct sorted elements.
        # @return None
        # @note If test_large_cases boolean is set to false, this test case will skip the execution of method to be
        # tested (execute_merge_sort) and always return True.
        #
        if test_large_cases:
            sort_object = Sort.SortingCSVData()
            sort_object.set_output_data(output_filename)
            sort_object.set_input_data(input_filename)
            sort_object.execute_merge_sort()

            Utils.python_built_in_sort(input_filename, expected_output_filename)  # Reference to compare
            self.assertEqual(True, filecmp.cmp(output_filename, expected_output_filename))
        else:
            self.assertEqual(1, 1)  # Return always True.

    # @data(('Lab_Resources/random_1G.csv', 'Lab_Resources/sorted_1G.csv', 'Lab_Resources/sorted_1G_expected.csv'))
    # @unpack
    # def test_sort_ridiculously_big_file_float_right(self, input_filename, output_filename, expected_output_filename):
    #    sort_object = Sort.SortingCSVData()
    #    sort_object.set_input_data(input_filename)
    #    sort_object.set_output_data(output_filename)
    #    sort_object.execute_merge_sort()

    #    Utils.python_built_in_sort(input_filename, expected_output_filename)        # Reference to compare
    #    self.assertEqual(True, filecmp.cmp(output_filename, expected_output_filename))

    def test_sort_corner_case(self):
        """
        Test the execute_merge_sort method from the SoringCVSData with correct input and output file names and just 1
        value to sort as a way to test the method under a corner case scenario.
        :return: None
        """
        Utils.generate_csv_file('Lab_Resources/random_int_1.csv', 1, 1, 0, 0, 10)
        sort_object = Sort.SortingCSVData()
        sort_object.set_input_data('Lab_Resources/random_int_1.csv')
        sort_object.set_output_data('Lab_Resources/sorted_int_1.csv')
        sort_object.execute_merge_sort()

        Utils.python_built_in_sort('Lab_Resources/random_int_1.csv', 'Lab_Resources/sorted_int_1_expected.csv')
        self.assertEqual(True, filecmp.cmp('Lab_Resources/sorted_int_1.csv', 'Lab_Resources/sorted_int_1_expected.csv'))

    def test_sort_no_values(self):
        """
        Test the execute_merge_sort method from the SoringCVSData with an empty file as input and a valId output file
        name. This test is expected to raise a custom exception for empty input file.
        :return: None
        """
        with open('Lab_Resources/empty.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow([])
        sort_object = Sort.SortingCSVData()
        sort_object.set_input_data('Lab_Resources/empty.csv')
        sort_object.set_output_data('Lab_Resources/sorted_empty.csv')

        self.assertRaises(Ex.EmptyFile, sort_object.execute_merge_sort, )

    @data(('Lab_Resources/random_int_1M.csv', 'Lab_Resources/sorted_int_1M.csv')
          # ,('Lab_Resources/random_int_10M.csv', 'Lab_Resources/sorted_int_10M.csv')
          )
    @unpack
    def test_performance_int(self, input_filename, output_filename):
        ##
        # Test the execute_merge_sort method from the SoringCVSData with valid input and output file names
        # and 1 million int values input for sort. It calculates the time it takes to complete the computation and
        # pass the test if it is less than a predetermined value of seconds; otherwise it fails.
        # @param input_filename File containing the values to sort
        # @return none
        # @param output_filename File name where the sorted values will be stored
        # @note If test_large_cases boolean is set to false, this test case will skip the execution of method to be
        # tested (execute_merge_sort) and always return True.
        #
        seconds = 300
        if test_large_cases:
            sort_object = Sort.SortingCSVData()
            sort_object.set_input_data(input_filename)
            sort_object.set_output_data(output_filename)
            start_time = time.time()
            sort_object.execute_merge_sort()
            execution_time = time.time() - start_time
            self.assertLess(execution_time, seconds)
        else:
            self.assertEqual(1, 1)  # Return always True.

    @data(('Lab_Resources/random_1M.csv', 'Lab_Resources/sorted_1M.csv')
          # ,('Lab_Resources/random_10M.csv', 'Lab_Resources/sorted_10M.csv', 'Lab_Resources/sorted_10M_expected.csv')
          )
    @unpack
    def test_performance_float(self, input_filename, output_filename):
        ##
        # Test the execute_merge_sort method from the SoringCVSData with valid input and output file names
        # and 1 million float values input for sort. It calculates the time it takes to complete the computation and
        # pass the test if it is less than a predetermined value of seconds; otherwise it fails.
        # @param input_filename File containing the values to sort
        # @param output_filename File name where the sorted values will be stored
        # @return none
        # @note If test_large_cases boolean is set to false, this test case will skip the execution of method to be
        # tested (execute_merge_sort) and always return True.
        #
        seconds = 300
        if test_large_cases:
            sort_object = Sort.SortingCSVData()
            sort_object.set_output_data(output_filename)
            sort_object.set_input_data(input_filename)
            start_time = time.time()
            sort_object.execute_merge_sort()
            execution_time = time.time() - start_time
            self.assertLess(execution_time, seconds)
        else:
            self.assertEqual(1, 1)  # Return always True.


## @defgroup main Main
#  @brief  This module correspond to the 'main' section of the project.
#
#  Configuration variables are set and function calls are performed in this module.

## @ingroup main
if __name__ == '__main__':
    """ Main entry point for the program. The execution of all test cases is performed here. """
    ## Boolean variable that determines if test cases with more than 1 million data are run.
    ## @note Set it to <CODE>False</CODE> for faster runs and debug
    test_large_cases = False

    unittest.main()         # Run all tests.
