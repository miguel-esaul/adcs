# TC4002.1 | ADCS Lab 2. #

This project was created as an assignment for the postgraduate-level course *TC4002.1 Software Analysis, Design and 
Construction* taught at Tecnológico de Monterrey Campus Guadalajara by Ph.D. Gerardo Padilla Zárate.

### Prerequisites ###

* Python 3.6 or above
* `Requirements.txt` file list all Python libraries this project depends on, install them using:

```
pip install -r requirements.txt
```

### Notes ###

* If using an IDE like Pycharm, it is recommended to create a Run Configuration using the `Lab2_main.py` as script instead of the default Python test that Pycharm creates when opening the project.

* Coverage results are available under /htmlcov/

* * *

### Description ###

Original instructions and requirements for this Lab can be found in 
[Instructions](https://bitbucket.org/miguel-esaul/adcs/src/master/Lab2/Lab%20Instructions/).

#### Goal ####

* The target of these exercises was to enable the student to practice and master programming skills during the course.

#### Requirements ####

* This program must implement unit test cases.
* Unit test cases must provide over 90% coverage.
* Python's built-in sort method can not be used.

#### Programming Exercises ####

The following section provides a brief description of the exercises required for this practice.

##### Exercise 10 #####
Create a class called myPowerList, implement methods for:
* Adding Items
* Removing the n-th item

##### Exercise 11 #####
Add a method in myPowerList to return the sorted list

##### Exercise 12 #####
Add a method in myPowerList to merge another list with the current list
* `Lmerge`(merge the list as prefix)
* `Rmerge` (merge the list as suffix)

##### Exercise 13 #####
Add a method in myPowerList to save the current list as a textfile 
* `saveToTextFile(filename)`

##### Exercise 14 #####
dd a method in myPowerList to read values for a List from a Textfile
* `readFromTextFile(filename)`

##### Exercise 15 #####
Create a class to manage a directory of users containing data: Name, Address, Phone, Email.
The class must be enable:

1. Creation of new record
1. Save all records in afile
1. Load records from a file
1. Search and get data from a given record

### Results ###

* 45 unit test cases were performed.
* 100% statement coverage.

* * *

### Authors ###

* **Miguel Avina** - *Initial Work* - [Bitbucket](https://bitbucket.org/miguel-esaul), 
[Github](https://github.com/miguelAvina1).

### License ###

The MIT License (MIT)

Copyright (c) 2020 Miguel Aviña Pantoja

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
