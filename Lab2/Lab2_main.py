import io
import os
import sys
from random import randrange
import unittest
from ddt import ddt, data, file_data, unpack


def value_swap(ls, index_a, index_b):
    """ Swaps two values in a list. """
    temp = ls[index_a]
    ls[index_a] = ls[index_b]
    ls[index_b] = temp


class UsersDirectory:
    def __init__(self):
        self.directory = []

    def add_user(self, name, address='Not Defined', phone='0',
                 email='user@something.com'):
        """ Adds a new entry to the directory. """
        self.directory.append([name, address, phone, email])

    def search(self, info):
        """ Search provided info in all the directory and returns first record number that contains it. """
        for record in self.directory:
            for data in record:
                if info == data:
                    return record
        return 'Not found in directory'

    def get_record(self, record_number):
        """ Returns the record given a record number. """
        return self.directory[record_number]

    def save_to_file_formatted(self, filename):
        """ Saves directory in a formatted way into a text file. """
        file = open(filename, 'w')
        for i, record in enumerate(self.directory):
            file.write(f'Record {i}\n')
            file.write(f'Name: {record[0]}\n')
            file.write(f'Address: {record[1]}\n')
            file.write(f'Phone: {record[2]}\n')
            file.write(f'Email: {record[3]}\n\n')
        file.close()

    def save_to_file(self, filename):
        """ Saves directory in a text file. """
        file = open(filename, 'w')
        for record in self.directory:
            for i in range(4):
                file.write(str(record[i])+'\n')
            file.write('\n')
        file.close()

    def load_from_formatted_file(self, filename):
        """ Loads directory data from a formatted text file. """
        file = open(filename, 'r')
        data = file.readline()
        while data != '':
            end_pos = data.find('\n')
            record = int(data[7:end_pos])
            data = file.readline()
            end_pos = data.find('\n')
            name = data[6:end_pos]
            data = file.readline()
            end_pos = data.find('\n')
            address = data[9:end_pos]
            data = file.readline()
            end_pos = data.find('\n')
            phone = data[7:end_pos]
            data = file.readline()
            end_pos = data.find('\n')
            email = data[7:end_pos]
            file.readline()
            self.directory.append([name, address, phone, email])
            data = file.readline()
        file.close()

    def load_from_file(self, filename):
        """ Loads directory data from text file. """
        file = open(filename, 'r')
        data = file.read().split('\n')
        record = 0
        i = 0
        while i < len(data) and data[i] != '':
            self.directory.append([str(val) for val in data[i:i+4]])
            record += 1
            i += 5
        file.close()

    def print_directory(self):
        """ Prints current directory. """
        print('Current directory:')
        for record in self.directory:
            print(str(record))


class MyPowerList:
    def __init__(self):
        self.ls = []

    def add_item(self, item):
        """ Adds item to current list. """
        self.ls.append(item)

    def remove_item(self, position):
        """ Remove n-th item from current list. """
        self.ls.pop(position)

    def sorted_list(self, sort_type='selectionSort'):
        """ Sorts current list given an specific algorithm. """
        if sort_type == 'selectionSort':
            sorted_list = self.ls.copy()
            for sorted_marker in range(1, len(sorted_list)):
                current_v = sorted_list[sorted_marker]
                i = sorted_marker
                while i > 0 and current_v < sorted_list[i - 1]:
                    sorted_list[i] = sorted_list[i - 1]  # Moving values to right
                    i -= 1
                sorted_list[i] = current_v
            return sorted_list
        elif sort_type == 'bubbleSort':
            sorted_list = self.ls.copy()
            i = 0
            while i < len(sorted_list) - 2:
                ordered = True
                for j in range(1, len(sorted_list) - i):
                    if sorted_list[j - 1] > sorted_list[j]:
                        ordered = False
                        value_swap(sorted_list, j - 1, j)
                if ordered:
                    return sorted_list
                else:
                    i += 1
            return sorted_list

    def merge_lists(self, new_list, selection):
        """ Merges two lists. """
        ls = self.ls.copy()
        newl = new_list.copy()
        if selection == 'Rmerge':
            ls.extend(newl)
            return ls
        elif selection == 'Lmerge':
            newl.extend(ls)
            return newl

    def save_to_text_file(self, filename, separator):
        """ Saves current list into a text file. """
        file = open(filename, 'w')
        ls = self.ls.copy()
        last_element = ls.pop(len(ls)-1)
        list_str = ''.join([str(item)+separator for item in ls])
        list_str += str(last_element)
        file.write(list_str)
        file.close()

    def read_from_text_file(self, filename, separator):
        """ Reads and save list from a text file. """
        sep_len = len(separator)
        file = open(filename, 'r')
        list_str = file.read()
        ls = ['']
        j = 0
        i = 0
        while i < len(list_str):
            if list_str[i] == separator[0]:
                j += 1          # new element in list
                i += sep_len    # ignoring separator
                ls.extend(list_str[i])      # adding first char in new element
            else:
                ls[j] = ls[j]+list_str[i]   # appending chars
            i += 1      # iterating through string
        file.close()
        ls = [int(n) for n in ls]
        self.ls = ls      # todo: return it or replace the list in object?
        # return ls


# Test Classes #########################################################################################################

@ddt
class TestingLab2(unittest.TestCase):
    @data((23, 12, 54, 87, 45, 99, 3), (120, 31, 96, 54, 451, 64, 663), (2, 7554, 55, 23, 99, 86, 6))
    def test_adding_item_ddt(self, values):
        test_list = MyPowerList()
        for val in values:
            test_list.add_item(val)
        self.assertEqual(test_list.ls, list(values))

    @data(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
    def test_remove_nth_item_ddt(self, value):
        test_list = MyPowerList()
        ls = []
        for i in range(10):
            item = randrange(100)
            test_list.add_item(item)
            if i != value:
                ls.append(item)
        test_list.remove_item(value)
        self.assertEqual(test_list.ls, ls)

    # @file_data("Lab_Resources/test_sorting_ddt.json")
    @data(1, 10, 100, 1000, 1000)
    def test_sorting_selection_sort(self, value):
        test_list = MyPowerList()
        for _ in range(value):
            test_list.add_item(randrange(1000000))
        self.assertEqual(test_list.sorted_list('selectionSort'), sorted(test_list.ls))

    @data(1, 10, 100, 1000, 1000)
    def test_sorting_bubble_sort(self, value):
        test_list = MyPowerList()
        for _ in range(value):
            test_list.add_item(randrange(1000000))
        self.assertEqual(test_list.sorted_list('bubbleSort'), sorted(test_list.ls))

    def test_RMerge(self):
        test_list = MyPowerList()
        new_list = []
        merged_list = []
        for _ in range(10):
            val = randrange(100)
            test_list.add_item(val)
            merged_list.append(val)
        for _ in range(10):
            val = randrange(100)
            new_list.append(val)
            merged_list.append(val)
        self.assertEqual(test_list.merge_lists(new_list, 'Rmerge'), merged_list)

    def test_LMerge(self):
        test_list = MyPowerList()
        new_list = []
        merged_list = []
        for _ in range(10):
            val = randrange(100)
            new_list.append(val)
            merged_list.append(val)
        for _ in range(10):
            val = randrange(100)
            test_list.add_item(val)
            merged_list.append(val)
        self.assertEqual(test_list.merge_lists(new_list, 'Lmerge'), merged_list)

    def test_creating_file_Lab2(self):
        test_list = MyPowerList()
        for _ in range(100):
            test_list.add_item(randrange(100))
        test_list.save_to_text_file('Lab_Resources/test_file_output.txt', ' ')
        self.assertEqual(True, os.path.isfile('Lab_Resources/test_file_output.txt'))
        os.remove('Lab_Resources/test_file_output.txt')     # Erasing the file

    def test_checking_file_contents_Lab2(self):  # Checking if the method creates the specified file
        test_list = MyPowerList()
        for _ in range(100):
            test_list.add_item(randrange(100))
        test_list.save_to_text_file('Lab_Resources/test_file_output.txt', ' ')
        file = open('Lab_Resources/test_file_output.txt')
        contents = file.read()
        contents = [int(value) for value in contents.split(' ')]       # List comprehension
        file.close()
        self.assertEqual(contents, test_list.ls)
        os.remove('Lab_Resources/test_file_output.txt')  # Erasing the file

    def test_read_from_file(self):
        test_list = MyPowerList()
        test_list.read_from_text_file('Lab_Resources/test_reading_contents.txt', ' ')
        file = open('Lab_Resources/test_reading_contents.txt')
        contents = file.read()
        file.close()
        self.assertEqual(test_list.ls, [int(value) for value in contents.split(' ')])

    @data(('Miguel', 'Juarez 23', 3316991715, 'A01227898@itesm.mx'), ('Esaul', 'Hidalgo 313', 6614227842,
                                                                      'miguelavina16@gmail.com'))
    @unpack
    def test_new_record_ddt_unpack(self, name, address, phone, email):
        dir1 = UsersDirectory()
        dir1.add_user(name, address, phone, email)
        self.assertEqual(dir1.directory, [[name, address, phone, email]])  # Testing if the method is saving the data

    def test_look_by_mail(self):
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        self.assertEqual(['Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com'],
                         dir1.search('miguelavina16@gmail.com'))

    def test_look_by_phone(self):
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        self.assertEqual(['Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx'], dir1.search('3316991715'))

    def test_look_not_existing(self):
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        self.assertEqual('Not found in directory', dir1.search(100))

    def test_get_record(self):
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        self.assertEqual(['Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com'], dir1.get_record(1))

    def test_creating_file_formatted(self):  # Checking if the method creates the specified file
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        dir1.save_to_file_formatted('Lab_Resources/f_testing_file.txt')
        self.assertEqual(True, os.path.isfile('Lab_Resources/f_testing_file.txt'))
        os.remove('Lab_Resources/f_testing_file.txt')  # Erasing the file

    def test_checking_file_contents_formatted(self):  # Checking if the method creates the specified file
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        dir1.save_to_file_formatted('Lab_Resources/f_testing_file.txt')
        file = open('Lab_Resources/f_testing_file.txt')
        contents = file.readlines()
        file.close()
        file = open('Lab_Resources/f_testing_file_expected_Lab2.txt')
        expected_contents = file.readlines()
        file.close()
        self.assertEqual(contents, expected_contents)
        os.remove('Lab_Resources/f_testing_file.txt')  # Erasing the file

    def test_creating_file(self):  # Checking if the method creates the specified file
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        dir1.save_to_file('Lab_Resources/uf_testing_file.txt')
        self.assertEqual(True, os.path.isfile('Lab_Resources/uf_testing_file.txt'))
        os.remove('Lab_Resources/uf_testing_file.txt')  # Erasing the file

    def test_checking_file_contents(self):  # Checking if the method creates the specified file
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        dir1.save_to_file('Lab_Resources/uf_testing_file.txt')
        file = open('Lab_Resources/uf_testing_file.txt')
        contents = file.readlines()
        file.close()
        file = open('Lab_Resources/uf_testing_file_expectedLab2.txt')
        expected_contents = file.readlines()
        file.close()
        self.assertEqual(contents, expected_contents)
        os.remove('Lab_Resources/uf_testing_file.txt')  # Erasing the file

    def test_load_file_formatted(self):
        dir1 = UsersDirectory()
        dir1.load_from_formatted_file('Lab_Resources/f_testing_file_expected_Lab2.txt')
        expected_data = [['Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx'],
                         ['Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com']]
        self.assertEqual(dir1.directory, expected_data)

    def test_load_file(self):
        dir1 = UsersDirectory()
        dir1.load_from_file('Lab_Resources/uf_testing_file_expectedLab2.txt')
        expected_data = [['Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx'],
                         ['Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com']]
        self.assertEqual(dir1.directory, expected_data)

    @file_data("Lab_Resources/test_directory_ddt_Lab2.json")
    def test_print_records(self, name, address, phone, email):
        dir1 = UsersDirectory()
        dir1.add_user(name, address, phone, email)
        captured_output = io.StringIO()
        sys.stdout = captured_output
        dir1.print_directory()
        sys.stdout = sys.__stdout__
        expected_output = "Current directory:\n['{}', '{}', {}, '{}']\n".format(name, address, phone, email)
        self.assertEqual(captured_output.getvalue(), expected_output)


if __name__ == '__main__':
    print("Lab 2. Analysis, design and development of software\n")
    power_list = MyPowerList()

    # Programming exercise 10: Add item and removing the n-th item #############
    print('\n---Programming exercise 10: Add items to the list---')
    for _ in range(10):         # Adding items
        rand = randrange(100)
        print(f'Adding {rand} to the list')
        power_list.add_item(rand)
    print(f'Current list: {power_list.ls}')

    print('\n---Programming exercise 10: Removing n-th items from list---')
    print(f'Current list: {power_list.ls}')
    for _ in range(3):
        rand = randrange(len(power_list.ls))
        power_list.remove_item(rand)   # Removing the n-th items
        print(f'Element {rand} removed. Current list: {power_list.ls}')

    # Programming exercise 11: Return the sorted list ##########################
    print('\n---Programming exercise 11: Return the sorted list---')
    sorted_ls = power_list.sorted_list('selectionSort')
    print(f'Sorting with selection Sort... \tSorted list: {sorted_ls}')
    sorted_ls = power_list.sorted_list('bubbleSort')
    print(f'Sorting with bubble Sort... \tSorted list: {sorted_ls}')

    # Programming exercise 12: Add a method in myPowerList to merge another list with the current list ####
    print('\n---Programming exercise 12: Add a method in myPowerList to merge another list with the current list---')
    second_list = [2, 4, 6, 444, 12]
    print(f'Second list to merge is: {second_list}')
    mergedL = power_list.merge_lists(second_list, 'Lmerge')
    mergedR = power_list.merge_lists(second_list, 'Rmerge')
    print(f'Second list merged as prefix: {mergedL}')
    print(f'Second list merged as suffix: {mergedR}')

    # Programming exercise 13: Add a method in myPowerList to save the current list as a text file #########
    print('\n---Programming exercise 13: Add a method in myPowerList to save the current list as a text file---')
    filename = 'Lab_Resources/My_list1.txt'
    separator = ', '
    print(f'Saving current list: {power_list.ls} to "{filename}" with separator "{separator}"')
    power_list.save_to_text_file(filename, separator)

    # Programming exercise 14: Add a method in myPowerList to read values for a List from a text file ######
    print('\n---Programming exercise 14: Add a method in myPowerList to read values for a List from a text file---')
    print(f'Reading list from "{filename}" with separator "{separator}"')
    power_list.read_from_text_file(filename, separator)
    print(f'Current list: {power_list.ls}')

    # Programming exercise 15: Create a class to manage a directory of users containing: Name, Address, Phone, Email
    print('\n---Programming exercise 15: Create a class to manage a directory of users containing: '
          'Name, Address, Phone, Email---')
    directory = UsersDirectory()

    # The class must be enable: 1. Creation of new record
    print('--The class must be enable: 1. Creation of new record--')
    print('Creating new records...')
    directory.add_user('Miguel', 'Arrayan 21', '3316991715', 'miguelavina@gmail.com')
    directory.add_user('Mariana', 'Pino Suarez 34', '6215475210', 'mariana_s12@outlook.com')
    directory.add_user('Roberto', 'Montera 1233', '8115962741', 'A01224568@itesm.com')
    directory.add_user('Sofia', 'Justo Sierra 11', '3511005547', 'sofia_123@hotmail.com')
    directory.print_directory()

    # The class must be enable: 2. Save all records in a file
    print('\n--The class must be enable: 2. Save all records in a file--')
    filename = 'Lab_Resources/Directory1_NoFormat.txt'
    print(f'Saving records without nice format in "{filename}"')
    directory.save_to_file(filename)
    filename = 'Lab_Resources/Directory1_Formatted.txt'
    print(f'Saving records with format in "{filename}"')
    directory.save_to_file_formatted(filename)

    # The class must be enable: 3. Load records from a file
    print('\n--The class must be enable: 3. Load records from a file--')
    filename = 'Lab_Resources/Directory1_NoFormat.txt'
    print(f'Loading un-formatted records from "{filename}"')
    directory.load_from_file('Lab_Resources/Directory1_NoFormat.txt')
    directory.print_directory()
    filename = 'Lab_Resources/Directory1_Formatted.txt'
    print(f'\nLoading formatted records from "{filename}"')
    directory.load_from_formatted_file('Lab_Resources/Directory1_Formatted.txt')
    directory.print_directory()

    # The class must be enable: 4. Search and get data from given record
    print('\n--The class must be enable: 4. Search and get data from given record--')
    record = 2
    print(f'Getting record {record}')
    rec = directory.get_record(record)
    print(rec)

    print(f'\nSearching data "3316991715":')
    rec = directory.search('3316991715')
    print(rec)

    print(f'\nSearching data "Sofia":')
    rec = directory.search('Sofia')
    print(rec)

    unittest.main()
