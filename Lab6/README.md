# TC4002.1 | ADCS Lab 6. #

This project was created as an assignment for the postgraduate-level course *TC4002.1 Software Analysis, Design and 
Construction* taught at Tecnológico de Monterrey Campus Guadalajara by Ph.D. Gerardo Padilla Zárate.

### Prerequisites ###

* Web browser 
* Windows OS to open .chm files


### Description ###

Original instructions and requirements for this Lab can be found in 
[Instructions](https://bitbucket.org/miguel-esaul/adcs/src/master/Lab6/Lab%20Instructions/).

#### Goals ####

* The target of these exercises was provide the students with the experience about generating documentation from source code.

* Expose the student with the challenges and also the opportunities by generating robust and good documentation from the source code

#### Requirements ####

* Generate documentation for two of the past labs.

### Results ###

* Lab1 and Lab5 were documented using Doxygen.
* Generated .html files for each labs are under Labn_Documentation/html folder.
* Compiled versions (.chm files) for windows are also provided.

* * *

### Authors ###

* **Miguel Avina** - *Initial Work* - [Bitbucket](https://bitbucket.org/miguel-esaul), 
[Github](https://github.com/miguelAvina1).

### License ###

The MIT License (MIT)

Copyright (c) 2020 Miguel Aviña Pantoja

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
