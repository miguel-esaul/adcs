
##  @mainpage
#   @{
#   @section Goal Goal
#   The target of these exercises was to enable the student to practice and master programming skills during the course.
#
#
#   @section requirements Requirements
#   - This program must implement unit test cases.
#   - Unit test cases must provide over 90% coverage.
#   - Python's built-in statistics method can not be used.
#
#
#   @section exercises Programming Exercises
#   The following section provides a brief description of the exercises required for this practice.
#
#   @subsection Exercise_8 Exercise 8
#   Write a module containing different function that computes the:
#   -# Sample mean.
#   -# Sample standard deviation.
#   -# Median.
#   -# A function that returns the n-quartile.
#   -# A function that returns the n-percentile.
#
#
#   @subsection Exercise_9 Exercise 9
#   Write a function that converts a decimal number into a Roman format.
#
#   @section results Results
#   - 61 unit test cases were performed.
#   - 100% statement coverage.
#
#   <br>
#   @note Two different types of comment blocks were used in the source code for demonstration purposes:
#   -# Standard <i>pythonic</i> way: Documentation strings (docstrings) used to describe classes, methods and functions.
#   In this case <b>none of doxygen's special commands are supported</b>. This is reflected in some member
#   functions documentation where the arguments and return value are not nicely formatted. See image below.
#   @image html unformatted_doc.png
#   -# Using C-like comments: Documentation comments starts with "##", more in line with they way documentation blocks
#   work for other language supported by doxygen, allowing the use of special commands. This is reflected in a nicely
#   formatted html output. See image below.
#   @image html formated_doc.png
#
#   <br><hr>
#   @author     Miguel Avi&ntilde;a
#   @date       March, 2020
#   @copyright  GNU Public License.
#
#   <br><hr>
#   @}
#

## @defgroup testing Testing Framework
#  @brief This module groups all classes and related functions regarding the unit tests for the whole project.
#
#  The unnittest built-in framework for python was used to test the sorting algorithm as well as the Lab5_utils.
#  Two different classes were used to test them, one for exercise 8 and another for exercise 9.
#  Also data driven test (DDT) was performed.

# Imports ordered based on PEP8 guidelines
# Standard library imports.
import unittest                         # System package
from random import randrange            # System package
import statistics                       # System package for unit test

# Related third party imports.
import numpy as np                      # Site packages
from ddt import ddt, data               # Site packages
import roman                            # Site packages

# Local application/library specific imports.
import statistics_module as stats       # Local module


##
# @defgroup Exercise_7 Exercise 7
# @brief This module groups all functions from the statistics module and the classes that test them.
#

## @ingroup Exercise_7
def exercise_7(file_name):
    ##
    # This function retrieves the data stored in the input file and use it to call all the functions
    # of the statistics_module module. The results of each function call are print in console.
    # @param file_name Name of the file with the input data.
    # @return None

    print('\nExercise 7.')

    file = open(file_name, 'r')
    data_set1 = file.read()
    file.close()
    data_set1 = [int(val) for val in data_set1.split()]

    # Executing functions of the statistics module  ########
    mean = stats.sample_mean(data_set1)
    std_dev = stats.sample_standard_deviation(data_set1)
    med = stats.median(data_set1)
    first_q = stats.n_quartile(data_set1, 1)
    second_q = stats.n_quartile(data_set1, 2)
    third_q = stats.n_quartile(data_set1, 3)
    percentile = 80
    n_percentile = stats.n_percentile(data_set1, percentile)
    print(f"Average mean is: {mean}.")
    print(f"Sample standard deviation is: {std_dev}")
    print(f"Median is: {med}")
    print(f"1st-quartile is: {first_q}")
    print(f"2nd-quartile is: {second_q}")
    print(f"3rd-quartile is: {third_q}")
    print(f"3rd-quartile is: {third_q}")
    print(f"{percentile}-percentile is: {n_percentile}")

##
# @defgroup Exercise_8 Exercise 8
# @brief This module groups all functions involved in the conversion from a cardinal integer number to roman format and
# the classes that test them.
#


## @ingroup Exercise_8
def int_to_roman(number):
    ##
    # This function performs the conversion from an integer value to a roman format.
    # @param number Number to be converted
    # @return String of the input number in roman format.
    roman_table = (1, 5, 10, 50, 100, 500, 1000, 5000, 10000)
    roman_numbers = ('I', 'V', 'X', 'L', 'C', 'D', 'M')
    index = 0
    if number >= 4000:
        return "number out of range (must be 0..4999)"
    while number >= roman_table[index]:
        index += 1

    index -= 1

    output = ''
    reminder = number
    while reminder > 0:
        reminder = number % roman_table[index]
        coefficient = number // roman_table[index]
        if coefficient == 4 or coefficient == 40 or coefficient == 400:
            output += roman_numbers[index] + roman_numbers[index + 1]
        elif coefficient == 1 and ((reminder == 4) or reminder // 10 == 4 or reminder // 100 == 4) and index % 2 == 1:
            output += roman_numbers[index - 1] + roman_numbers[index + 1]
            if index < 3:
                reminder -= 4
            elif index < 5:
                reminder -= 40
            else:
                reminder -= 400
        else:
            output += (roman_numbers[index] * coefficient)
        number = reminder
        index -= 1
    return output


## @ingroup Exercise_8
def exercise_8(number):
    ##
    # This function prints the exercise number the program is executing, the cardinal input number to be converted and
    # the roman format output.
    # @param number Number to be converted
    # @return None
    print('\nExercise 8.')
    print(number)
    output = int_to_roman(number)
    print(output)


## @ingroup testing Exercise_7

##
# This class contains all the methods that implement unit testing to the exercise 7 functions.
#
@ddt
class TestingLab1Exercise7(unittest.TestCase):
    def test_mean(self):
        ##
        # Test the mean function from the statistics_module.
        # The statistics_module.main function is called using valid parameters and then the return value of the function
        # is compared against python's built-in statistics module.
        #
        # @param self
        # @return None
        file = open('Lab_Resources/L1_dataSet1.txt', 'r')
        data_set1 = file.read()
        file.close()
        data_set1 = [int(val) for val in data_set1.split()]
        self.assertEqual(stats.sample_mean(data_set1), statistics.mean(data_set1))

    def test_median(self):
        ##
        # Test the median function from the statistics_module.
        # The statistics_module.median function is called using valid parameters and then the return value of the
        # function is compared against python's built-in statistics module.
        #
        # @param self
        # @return None
        file = open('Lab_Resources/L1_dataSet1.txt', 'r')
        data_set1 = file.read()
        file.close()
        data_set1 = [int(val) for val in data_set1.split()]
        self.assertEqual(stats.median(data_set1), statistics.median(data_set1))

    def test_median_odd(self):
        """
        Test the median function from the statistics_module.
        The statistics_module.median function is called using valid parameters and an odd number of data;
        then the return value of the function is compared against python's built-in statistics module.
        :return:
        """
        file = open('Lab_Resources/L1_dataSet1.txt', 'r')
        data_set1 = file.read()
        file.close()
        data_set1 = [int(val) for val in data_set1.split()]
        data_set1.pop(0)                      # making it odd LIST
        self.assertEqual(stats.median(data_set1), statistics.median(data_set1))

    @data(1, 10, 15, 20, 25, 30, 40, 50, 60, 70, 75, 90, 99, 100)
    def test_n_percentile(self, n_percentile):
        ##
        # Test the n_percentile function from the statistics_module.
        # The statistics_module.n_percentile function is called using valid parameters and then the return value of the
        # function is compared against python's built-in statistics module. 14 percentile values ranging from 1 to 100
        # were used to test this method using DDT.
        #
        # @param n_percentile N-percentile to compute
        # @return None
        file = open('Lab_Resources/L1_dataSet1.txt', 'r')
        data_set1 = file.read()
        file.close()
        data_set1 = data_set1.split()
        data_set1 = [int(val) for val in data_set1]
        self.assertAlmostEqual(stats.n_percentile(data_set1, n_percentile),
                               round(np.percentile(data_set1, n_percentile)), -2)  # Ok, this is too much range of error

    @data('Lab_Resources/L1_dataSet1.txt', 'Lab_Resources/L1_dataSet2.txt')
    def test_arrange_values(self, filename):
        """
        Test the arrange_values function from the statistics_module.
        The statistics_module.arrange_values function is called using valid parameters, then the return value of the
        function is compared against python's built-in sorted function.
        :param: filename: input file name
        :return: None
        """
        file = open(filename)
        data_set1 = file.read()
        file.close()
        data_set1 = data_set1.split()
        data_set1 = [int(val) for val in data_set1]
        self.assertEqual(stats.arrange_values(data_set1), sorted(data_set1))

    @data(1, 2, 3)
    def test_n_quartile(self, n_quartile):
        ##
        # Test the n_quartile function from the statistics_module.
        # The statistics_module.quartile function is called using valid parameters and then the return value of the
        # function is compared against python's built-in statistics module. All three posible quartile values
        # were used to test this method using DDT.
        #
        # @param n_quartile N-quartile to compute
        # @return None
        file = open('Lab_Resources/L1_dataSet1.txt', 'r')
        data_set1 = file.read()
        file.close()
        data_set1 = [int(val) for val in data_set1.split()]
        self.assertAlmostEqual(stats.n_quartile(data_set1, n_quartile),
                               np.quantile(data_set1, n_quartile*0.25), -2)  # Ok, this is too much range of error

    def test_n_quartile_failed(self):
        ##
        # Test the n_quartile function from the statistics_module.
        # The statistics_module.quartile function is called using invalid parameters and then the return value of the
        # function is checked. Expected return value is 'Failed'.
        #
        # @param self
        # @return None
        file = open('Lab_Resources/L1_dataSet1.txt', 'r')
        data_set1 = file.read()
        file.close()
        data_set1 = [int(val) for val in data_set1.split()]
        self.assertAlmostEqual(stats.n_quartile(data_set1, 5), 'Failed')

    @data('Lab_Resources/L1_dataSet1.txt', 'Lab_Resources/L1_dataSet2.txt')
    def test_sample_std_deviation(self, filename):
        """
        Test the sample_standard_deviation function from the statistics_module.
        The statistics_module.sample_standard_deviation function is called using valid parameters,
        then the return value of the function is compared against python's built-in statistics module,
        :param: filename: input file name
        :return: None
        """
        file = open(filename)
        data_set1 = file.read()
        file.close()
        data_set1 = [int(val) for val in data_set1.split()]
        self.assertAlmostEqual(stats.sample_standard_deviation(data_set1), np.std(data_set1), -2)


## @ingroup testing Exercise_8
@ddt
class TestingLab1Exercise8(unittest.TestCase):
    @data(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 17, 19, 20, 40, 45, 50, 55, 80, 90, 100, 120, 220, 400, 500,
          600, 700, 900, 1000, 2000, 3500, 3999)
    def test_all_possible(self, number):
        ##
        # Test the int_to_roman function.
        # The int_to_roman function is called using valid parameters and then the return value of the
        # function is compared against python's built-in roman module. 35 numbers ranging from 1 to 3999
        # were used to test this method using DDT.
        #
        # @param number Integer number to convert.
        # @return None
        self.assertEqual(int_to_roman(number), roman.toRoman(number))

    def test_out_of_range(self):
        ##
        # Test the int_to_roman function.
        # The int_to_roman function is called using an invalid parameter and then the return value of the
        # function is checked. Expected return value is 'number out of range (must be 0..4999)'.
        #
        # @param self
        # @return None
        self.assertEqual(int_to_roman(5000), 'number out of range (must be 0..4999)')


## @defgroup main Main
#  @brief  This module correspond to the 'main' section of the project.
#
#  Configuration variables are set and function calls are performed in this module.

##
# @ingroup main
# @{
if __name__ == '__main__':

    ## First Input file name with relative path that will be used for exercise 7.
    input_file_1 = 'Lab_Resources/L1_dataSet1.txt'

    ## Second Input file name with relative path that will be used for exercise 7.
    input_file_2 = 'Lab_Resources/L1_dataSet2.txt'

    exercise_7(input_file_1)
    exercise_7(input_file_2)

    ## Number of random values generated to test the integer to roman functionality of exercise 9.
    number_of_cycles = 100

    ## Maximum number that can be randomly generated to test the integer to roman functionality of exercise 9.
    # @note The maximum valid value is 4000.
    maximum_random_value = 4000

    for _ in range(number_of_cycles):
        exercise_8(randrange(maximum_random_value))

    unittest.main()

##
# @}
