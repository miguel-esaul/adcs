# TC4002.1 | ADCS Lab 1. #

This project was created as an assignment for the postgraduate-level course *TC4002.1 Software Analysis, Design and 
Construction* taught at Tecnológico de Monterrey Campus Guadalajara by Ph.D. Gerardo Padilla Zárate.

### Prerequisites ###

* Python 3.6 or above
* `Requirements.txt` file list all Python libraries this project depends on, install them using:

```
pip install -r requirements.txt
```

### Notes ###

* If using an IDE like Pycharm, it is recommended to create a Run Configuration using the `Lab1_main.py` as script instead of the default Python test that Pycharm creates when opening the project.

* Documentation for this lab can be find under /html/index.html. Also a compiled version is provided: `Lab1 Help.chm`.

* Coverage results are available under /htmlcov/

* * *

### Description ###

Original instructions and requirements for this Lab can be found in 
[Instructions](https://bitbucket.org/miguel-esaul/adcs/src/master/Lab1/Lab%20Instructions/).

#### Goal ####

* The target of these exercises was to enable the student to practice and master programming skills during the course.

#### Requirements ####

* This program must implement unit test cases.
* Unit test cases must provide over 90% coverage.
* Python's built-in statistics method can not be used

#### Programming Exercises ####

The following section provides a brief description of the exercises required for this practice.

##### Exercise 8 #####

Write a module containing different function that computes the:

1. Sample mean.
1. Sample standard deviation.
1. Median.
1. A function that returns the n-quartile.
1. A function that returns the n-percentile.

##### Exercise 9 #####

Write a function that converts a decimal number into a Roman format.

### Results ###

* 61 unit test cases were performed.
* 100% statement coverage.

* * *

### Authors ###

* **Miguel Avina** - *Initial Work* - [Bitbucket](https://bitbucket.org/miguel-esaul), 
[Github](https://github.com/miguelAvina1).

### License ###

The MIT License (MIT)

Copyright (c) 2020 Miguel Aviña Pantoja

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
