# Write a module containing different function that computes the
# 1.Sample mean
# 2.Sample standard deviation
# 3.Median
# 4.A function that returns the n-quartil
# 5.A function that returns the n-percentil


## @ingroup Exercise_7
def sample_mean(values):
    ##
    # This function the sample mean of an iterable object
    # @param values iterable object containing numerical elements.
    # @return Sample mean of input values
    #
    return sum(values)/len(values)


## @ingroup Exercise_7
def sample_standard_deviation(values):
    """ This function computes the sample standard deviation of an iterable object

        Parameters: values : iterable object
        Returns: double :  sample standard deviation
       """
    mean = sample_mean(values)
    summation = 0
    for num in values:
        summation += (num - mean)**2
    std_dev = (summation/(len(values)-1))**0.5

    return std_dev


## @ingroup Exercise_7
def median(values):
    """ This function computes the median of an iterable object

        Parameters: values : iterable object
        Returns: double :  median
       """
    arr_values = arrange_values(values)
    index = (len(arr_values) // 2) - 1     # integer division
    if len(arr_values) % 2 == 0:            # Even number of values
        med = (arr_values[index] + arr_values[index+1]) / 2
    else:
        med = arr_values[index + 1]

    return med


## @ingroup Exercise_7
def n_quartile(values, n):
    """ This function computes the n-quartile of an iterable object

        Parameters: values : iterable object
                    n      : quartile
        Returns: double :  n-quartile
       """
    if n == 1:  # First quartile
        index = (len(values) + 1) // 4
        ans = arrange_values(values)[index]
    elif n == 3:  # Third quartile
        index = 3*(len(values) + 1) // 4
        ans = arrange_values(values)[index]
    elif n == 2:  # Second quartile
        ans = median(values)
    else:
        ans = 'Failed'
    return ans


## @ingroup Exercise_7
def n_percentile(values, n):
    ##
    # This function computes the n-percentile of an iterable object
    # @param values iterable object containing numerical elements.
    # @param n Percentile to be computed.
    # @return n-percentile of the iterable object.
    #
    new_val = arrange_values(values)
    if n == 100:        # specific case
        index = len(values) - 1
    else:
        index = (n*0.01)*len(values)
        index = round(index)
    return new_val[index]


## @ingroup Exercise_7
def arrange_values(values):
    """ This function arrange the values of an iterable object

        Parameters: values : iterable object
        Returns: list :  ordered values
       """
    temp = values.copy()
    output = []
    while len(temp) > 0:
        m = min(temp)         # gets minimum value of list
        i = temp.index(m)     # gets minimum values index in list
        output.append(temp.pop(i))
    return output