import io
import logging
import os
import sys
import unittest
from itertools import count
from ddt import ddt, data, unpack, file_data

logger = logging.getLogger(f'__main__.{__name__}')   # Creating child logger


class UsersDirectory:
    _ids = count(0)

    def __init__(self):
        self.logger = logging.getLogger(f'__main__.{__name__}.UsersDirectory')      # Creating child logger for Class
        self.id = next(self._ids)
        self.directory = []
        self.logger.info(f'UsersDirectory instance created. Instance ID: {self.id}')

    def add_user(self, name, address='Not Defined', phone='0',
                 email='user@something.com'):
        """ Adds a new entry to the directory. """
        self.logger.info(f'{name} added to directory')
        self.directory.append([name, address, phone, email])

    def search(self, info):
        """ Search provided info in all the directory and returns first record number that contains it. """
        self.logger.info(f'Executing a search with: "{info}" as input information')
        for record in self.directory:
            for data in record:
                if info == data:
                    return record
        self.logger.info(f'Search with "{info}" did not result in any record')
        return 'Not found in directory'

    def get_record(self, record_number):
        self.logger.info(f'Retrieving record from directory using record number {record_number}')
        """ Returns the record given a record number. """
        return self.directory[record_number]

    def save_to_file_formatted(self, filename):
        """ Saves directory in a formatted way into a text file. """
        self.logger.info(f'Opening file "{filename}"')
        file = open(filename, 'w')
        self.logger.info(f'Writing all directory records to "{filename}" in a formatted way')
        for i, record in enumerate(self.directory):
            file.write(f'Record {i}\n')
            file.write(f'Name: {record[0]}\n')
            file.write(f'Address: {record[1]}\n')
            file.write(f'Phone: {record[2]}\n')
            file.write(f'Email: {record[3]}\n\n')
        self.logger.info(f'Closing file "{filename}"')
        file.close()

    def save_to_file(self, filename):
        """ Saves directory in a text file. """
        self.logger.info(f'Opening file "{filename}"')
        file = open(filename, 'w')
        self.logger.info(f'Writing all directory records to "{filename}"')
        for record in self.directory:
            for i in range(4):
                file.write(str(record[i])+'\n')
            file.write('\n')
        self.logger.info(f'Closing file "{filename}"')
        file.close()

    def load_from_formatted_file(self, filename):
        """ Loads directory data from a formatted text file. """
        self.logger.info(f'Opening file "{filename}"')
        file = open(filename, 'r')
        self.logger.info(f'Retrieving data from "{filename}"')
        data = file.readline()
        while data != '':
            end_pos = data.find('\n')
            record = int(data[7:end_pos])
            data = file.readline()
            end_pos = data.find('\n')
            name = data[6:end_pos]
            data = file.readline()
            end_pos = data.find('\n')
            address = data[9:end_pos]
            data = file.readline()
            end_pos = data.find('\n')
            phone = data[7:end_pos]
            data = file.readline()
            end_pos = data.find('\n')
            email = data[7:end_pos]
            file.readline()
            self.directory.append([name, address, phone, email])
            data = file.readline()
        self.logger.info(f'Closing file "{filename}"')
        file.close()

    def load_from_file(self, filename):
        """ Loads directory data from text file. """
        self.logger.info(f'Opening file "{filename}"')
        file = open(filename, 'r')
        self.logger.info(f'Retrieving data from "{filename}"')
        data = file.read().split('\n')
        record = 0
        i = 0
        while i < len(data) and data[i] != '':
            self.directory.append([str(val) for val in data[i:i+4]])
            record += 1
            i += 5
        self.logger.info(f'Closing file "{filename}"')
        file.close()

    def print_directory(self):
        """ Prints current directory. """
        self.logger.info(f'Printing current directory')
        print('Current directory:')
        for record in self.directory:
            print(str(record))


# Test Class ###########################################################################################################

@ddt
class DirectoryTestClass(unittest.TestCase):

    @data(('Miguel', 'Juarez 23', 3316991715, 'A01227898@itesm.mx'), ('Esaul', 'Hidalgo 313', 6614227842,
                                                                      'miguelavina16@gmail.com'))
    @unpack
    def test_new_record_ddt_unpack(self, name, address, phone, email):
        logger.info('Testing adding new record')
        dir1 = UsersDirectory()
        dir1.add_user(name, address, phone, email)
        self.assertEqual(dir1.directory, [[name, address, phone, email]])  # Testing if the method is saving the data

    def test_look_by_mail(self):
        logger.info('Testing searching by mail')
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        self.assertEqual(['Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com'],
                         dir1.search('miguelavina16@gmail.com'))

    def test_look_by_phone(self):
        logger.info('Testing searching by phone')
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        self.assertEqual(['Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx'], dir1.search('3316991715'))

    def test_look_not_existing(self):
        logger.info('Testing searching non existing record')
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        self.assertEqual('Not found in directory', dir1.search(100))

    def test_get_record(self):
        logger.info('Testing getting a record from directory')
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        self.assertEqual(['Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com'], dir1.get_record(1))

    def test_creating_file_formatted(self):  # Checking if the method creates the specified file
        logger.info('Testing creating a formatted file')
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        dir1.save_to_file_formatted('Lab_Resources/f_testing_file.txt')
        self.assertEqual(True, os.path.isfile('Lab_Resources/f_testing_file.txt'))
        os.remove('Lab_Resources/f_testing_file.txt')  # Erasing the file

    def test_checking_file_contents_formatted(self):  # Checking if the method creates the specified file
        logger.info('Testing checking contents from a formatted file')
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        dir1.save_to_file_formatted('Lab_Resources/f_testing_file.txt')
        file = open('Lab_Resources/f_testing_file.txt')
        contents = file.readlines()
        file.close()
        file = open('Lab_Resources/f_testing_file_expected_Lab2.txt')
        expected_contents = file.readlines()
        file.close()
        self.assertEqual(contents, expected_contents)
        os.remove('Lab_Resources/f_testing_file.txt')  # Erasing the file

    def test_creating_file(self):  # Checking if the method creates the specified file
        logger.info('Testing creating a file')
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        dir1.save_to_file('Lab_Resources/uf_testing_file.txt')
        self.assertEqual(True, os.path.isfile('Lab_Resources/uf_testing_file.txt'))
        os.remove('Lab_Resources/uf_testing_file.txt')  # Erasing the file

    def test_checking_file_contents(self):
        logger.info('Testing checking file contents')
        dir1 = UsersDirectory()
        dir1.add_user('Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx')
        dir1.add_user('Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com')
        dir1.save_to_file('Lab_Resources/uf_testing_file.txt')
        file = open('Lab_Resources/uf_testing_file.txt')
        contents = file.readlines()
        file.close()
        file = open('Lab_Resources/uf_testing_file_expectedLab2.txt')
        expected_contents = file.readlines()
        file.close()
        self.assertEqual(contents, expected_contents)
        os.remove('Lab_Resources/uf_testing_file.txt')  # Erasing the file

    def test_load_file_formatted(self):
        logger.info('Testing loading contents from a formatted file')
        dir1 = UsersDirectory()
        dir1.load_from_formatted_file('Lab_Resources/f_testing_file_expected_Lab2.txt')
        expected_data = [['Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx'],
                         ['Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com']]
        self.assertEqual(dir1.directory, expected_data)

    def test_load_file(self):
        logger.info('Testing loading contents from a file')
        dir1 = UsersDirectory()
        dir1.load_from_file('Lab_Resources/uf_testing_file_expectedLab2.txt')
        expected_data = [['Miguel', 'Juarez 23', '3316991715', 'A01227898@itesm.mx'],
                         ['Esaul', 'Hidalgo 313', '6614227842', 'miguelavina16@gmail.com']]
        self.assertEqual(dir1.directory, expected_data)

    @file_data("Lab_Resources/test_directory_ddt_Lab2.json")
    def test_print_records(self, name, address, phone, email):
        logger.info('Testing printing records on console')
        dir1 = UsersDirectory()
        dir1.add_user(name, address, phone, email)
        captured_output = io.StringIO()
        sys.stdout = captured_output
        dir1.print_directory()
        sys.stdout = sys.__stdout__
        expected_output = "Current directory:\n['{}', '{}', {}, '{}']\n".format(name, address, phone, email)
        self.assertEqual(captured_output.getvalue(), expected_output)


def run_lab():
    logger.info('Executing Lab2 with console output strings for user')
    print("Lab 2. Analysis, design and development of software\n")

    # Programming exercise 15: Create a class to manage a directory of users containing: Name, Address, Phone, Email
    print('\n---Programming exercise 15: Create a class to manage a directory of users containing: '
          'Name, Address, Phone, Email---')
    directory = UsersDirectory()

    # The class must be enable: 1. Creation of new record
    print('--The class must be enable: 1. Creation of new record--')
    print('Creating new records...')
    directory.add_user('Miguel', 'Arrayan 21', '3316991715', 'miguelavina@gmail.com')
    directory.add_user('Mariana', 'Pino Suarez 34', '6215475210', 'mariana_s12@outlook.com')
    directory.add_user('Roberto', 'Montera 1233', '8115962741', 'A01224568@itesm.com')
    directory.add_user('Sofia', 'Justo Sierra 11', '3511005547', 'sofia_123@hotmail.com')
    directory.print_directory()

    # The class must be enable: 2. Save all records in a file
    print('\n--The class must be enable: 2. Save all records in a file--')
    filename = 'Lab_Resources/Directory1_NoFormat.txt'
    print(f'Saving records without nice format in "{filename}"')
    directory.save_to_file(filename)
    filename = 'Lab_Resources/Directory1_Formatted.txt'
    print(f'Saving records with format in "{filename}"')
    directory.save_to_file_formatted(filename)

    # The class must be enable: 3. Load records from a file
    print('\n--The class must be enable: 3. Load records from a file--')
    filename = 'Lab_Resources/Directory1_NoFormat.txt'
    print(f'Loading un-formatted records from "{filename}"')
    directory.load_from_file('Lab_Resources/Directory1_NoFormat.txt')
    directory.print_directory()
    filename = 'Lab_Resources/Directory1_Formatted.txt'
    print(f'\nLoading formatted records from "{filename}"')
    directory.load_from_formatted_file('Lab_Resources/Directory1_Formatted.txt')
    directory.print_directory()

    # The class must be enable: 4. Search and get data from given record
    print('\n--The class must be enable: 4. Search and get data from given record--')
    record = 2
    print(f'Getting record {record}')
    rec = directory.get_record(record)
    print(rec)

    print(f'\nSearching data "3316991715":')
    rec = directory.search('3316991715')
    print(rec)

    print(f'\nSearching data "Sofia":')
    rec = directory.search('Sofia')
    print(rec)

    logger.info('Executing unit test for Lab with exit=False')
    unittest.main(__name__, exit=False)
