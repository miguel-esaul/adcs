import csv
import logging
import unittest
from ddt import data, unpack, ddt
import CustomExceptions as Ex
import os
from stat import S_IREAD, S_IRGRP, S_IROTH, S_IWUSR
import Lab5_utils as Utils
import L5_Sorting as Sort


logger = logging.getLogger(f'__main__.{__name__}')   # Creating child logger


@ddt
class TestingUtilsFunctions(unittest.TestCase):
    """ Tester class for the Lab5_utils module """
    @data('Lab_Resources/Example1.csv', 'Lab_Resources/Example2.csv')
    def test_generate_file(self, filename):
        """
        Test the generate_cvs_file function from the Utils module with expected parameter types.
        :param filename: string name of the file
        :return: None
        """
        logger.info(f'Testing generation of new cvs file with name {filename}')
        Utils.generate_csv_file(filename, 10, 10, 2, 0, 100)
        self.assertEqual(True, os.path.isfile(filename))
        logger.info(f'Removing {filename} file from system')
        os.remove(filename)

    @data(82, 755, 1004.4)
    def test_generate_file_bad_input(self, filename):
        logger.info(f'Testing generation of file with non valid input parameters: {filename} as filename')
        self.assertRaises(Ex.IncorrectType, Utils.generate_csv_file, filename, 10, 10, 2, 0, 100)

    @data(('F', 123, 54), (12, 545, 1), (1, 4, 'a'))
    @unpack
    def test_generate_file_missing_arg(self, arg1, arg2, arg3):
        logger.info(f'Testing generation of file with arguments: {arg1}, {arg2}, {arg3}')
        self.assertRaises(TypeError, Utils.generate_csv_file, arg1, arg2, arg3)


@ddt
class TestingSortingClass(unittest.TestCase):
    """
    Tester class for the Lab5_sortingClass module.
    """
    def test_not_input_specified(self):
        """
        Test the case where no input file is specified and execute merge sort is called.
        :return: None
        """
        logger.info('Testing sorting method without a specified input file')
        sort_object = Sort.SortingCSVData()
        self.assertRaises(Ex.InputFileNotSpecified, sort_object.execute_merge_sort)

    @data(82, 755, 1004.4)
    def test_invalid_input_file(self, filename):
        """
        Test set_input_data method from SortingCVSData class when incorrect input types are given.
        :return: None
        """
        logger.info(f'Testing of set_input_data with invalid filename :{filename}')
        sort_object = Sort.SortingCSVData()
        self.assertRaises(Ex.IncorrectType, sort_object.set_input_data, filename)

    @data('Lab_Resources/File1.csv', 'Lab_Resources/File2.csv', 'Lab_Resources/File3.csv')
    def test_not_existing_input(self, filename):
        """
        Test set_input_data method from SortingCVSData class when correct input types are given but files are missing
        in the current working directory.
        :param filename: name of the file
        :return: None
        """
        logger.info(f'Testing of set_input_data with filename: "{filename}" but file missing in working directory')
        sort_object = Sort.SortingCSVData()
        self.assertRaises(Ex.CVSFileNotFound, sort_object.set_input_data, filename)

    @data('Lab_Resources/File1_output.csv', 'Lab_Resources/File2_output.csv', 'Lab_Resources/File3_output.csv')
    def test_set_output(self, filename):
        """
        Test set_output_data method from SortingCVSData class with correct and valid filenames.
        :param filename: name of the file
        :return: None
        """
        logger.info(f'Testing of set_output_data with correct valid filename: {filename}')
        sort_object = Sort.SortingCSVData()
        sort_object.set_output_data(filename)
        self.assertEqual(sort_object.output_file_path, filename)

    def test_no_permission_output(self):
        """
        Test set_output_data method from SortingCVSData for a "read-only" file.
        :return: None
        """
        logger.info(f'Testing of set_output_data with a read-only file as argument')
        logger.info('Generating new cvs file')
        with open('Lab_Resources/read_only_file.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["SN", "Name", "Contribution"])
        filename = "Lab_Resources/read_only_file.csv"
        logger.info('Changing permissions of generated cvs file to read-only')
        os.chmod(filename, S_IREAD | S_IRGRP | S_IROTH)
        sort_object = Sort.SortingCSVData()
        logger.info('Calling set_output_data with read-only file')
        self.assertRaises(Ex.InvalidFile, sort_object.set_output_data, filename)
        logger.info('Removing read-only file')
        os.chmod(filename, S_IWUSR | S_IREAD)  # This makes the file read/write for the owner
        os.remove("Lab_Resources/read_only_file.csv")

    @data(82, 755, 1004.4)
    def test_invalid_output_file(self, filename):
        """
        Test set_output_data method from SortingCVSData class when incorrect input types are given.
        :return: None
        """
        logger.info(f'Testing of set_output_data with invalid filename :{filename}')
        sort_object = Sort.SortingCSVData()
        self.assertRaises(Ex.IncorrectType, sort_object.set_output_data, filename)

    def test_built_in_type_error_output(self):
        """
        Test set_output_data method from SortingCVSData when no arguments are given and a Type error is expected.
        :return: None
        """
        logger.info('Testing set_output_data without arguments')
        sort_object = Sort.SortingCSVData()
        self.assertRaises(TypeError, sort_object.set_output_data, )

    def test_generic_error_output(self):
        """
        Test set_output_data method from SortingCVSData when no arguments are given and a custom error is expected.
        :return: None
        """
        logger.info('Testing set_output_data with "\'\'" as arguments')
        sort_object = Sort.SortingCSVData()
        self.assertRaises(Ex.Error, sort_object.set_output_data, '')


def run_lab():
    """ Main entry point for the program. The execution of all test cases is performed here. """
    logger.info('Executing unit test for Lab')
    unittest.main(__name__, exit=False)
