from random import uniform
import logging
import CustomExceptions as Ex
import csv

logger = logging.getLogger(f'__main__.Lab5.{__name__}')   # Creating child logger


def generate_csv_file(filename, rows, columns, decimals, lower_l, upper_l):
    logger.info(f'Executing generate_cvs_file function with arguments {filename}, {rows}, {columns}, {decimals}, '
                f'{lower_l}, {upper_l}')
    if type(filename) != str:
        logger.error(f'Incorrect filename. String argument expected, given: {type(filename)}: {filename}')
        raise Ex.IncorrectType  # Custom exception
    else:
        logger.info(f'Generating cvs file with arguments {filename}, {rows}, {columns}, {decimals}, {lower_l}, '
                    f'{upper_l}')
        with open(filename, 'w', newline='') as file:
            writer = csv.writer(file)
            for _ in range(rows):        # Number of rows
                writer.writerow(['%.{}f'.format(decimals) % (uniform(lower_l, upper_l)) for _ in range(columns)])


def save_data_to_csv(data, output_filename):
    logger.info(f'Executing save_data_to_csv function with arguments {data}, {output_filename}')
    with open(output_filename, 'w', newline='') as file:
        writer = csv.writer(file)
        if len(data) < 10:
            rows = 1
            columns = len(data)
        else:
            rows = len(data) // 10
            columns = 10
        i = 0
        for _ in range(rows):  # Number of rows
            writer.writerow(data[i:i + columns])
            i += 10
