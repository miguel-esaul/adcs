## @defgroup exceptions Custom Exceptions
#  @brief This module groups all the custom exception classes used in some of the unit tests.


## @ingroup exceptions
##
# @brief Base class for other custom exceptions.
class Error(Exception):
    """ Base class for other custom exceptions. """


## @ingroup exceptions
##
# @brief Raised when specified filename is not found.
class CVSFileNotFound(Error):
    """ Raised when specified filename is not found. """


## @ingroup exceptions
##
# @brief Raised when another type of input different than string is given.
class IncorrectType(Error):
    """ Raised when another type of input different than string is given. """


## @ingroup exceptions
##
# @brief Raised when trying to sort the data but input file is missing.
class InputFileNotSpecified(Error):
    """ Raised when trying to sort the data but input file is missing. """


## @ingroup exceptions
##
# @brief Raised when trying write on an invalid file.
class InvalidFile(Error):
    """ Raised when trying write on an invalid file. """


## @ingroup exceptions
##
# @brief Raised when trying to sort values from an empty file.
class EmptyFile(Error):
    """ Raised when trying to sort values from an empty file. """

