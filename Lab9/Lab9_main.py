import logging
import Lab2
import Lab5


def configure_logger(level):
    logger = logging.getLogger(__name__)

    # Following part is heavily based on an example implementation from https://realpython.com/python-logging/
    # Create handlers
    c_handler = logging.StreamHandler()  # Emit to console
    f_handler = logging.FileHandler('ApplicationLog.log', 'a')  # Emit to file

    # Set handlers' levels
    c_handler.setLevel(logging.INFO)  # This handler will catch logRecords from INFO level
    f_handler.setLevel(logging.INFO)  # Also catch logRecords from INFO to ERROR but emit to different destination

    # Create formatters
    c_format = logging.Formatter('%(asctime)s Process %(process)d  %(levelname)s  \t %(name)s -- %(message)s.')
    f_format = logging.Formatter(
        '%(asctime)s - %(process)d - %(levelname)s - %(processName)s - %(name)s - - %(message)s. '
        'At line %(lineno)d')

    # Add formatter to handlers
    c_handler.setFormatter(c_format)
    f_handler.setFormatter(f_format)

    # Add handlers to logger
    logger.addHandler(c_handler)
    logger.addHandler(f_handler)

    logger.setLevel(level)  # Change default WARNING level
    return logger


if __name__ == '__main__':
    APP_LOG_LEVEL = 'INFO'
    logger = configure_logger(APP_LOG_LEVEL)   # With new logging module this is possible instead of logging.INFO
    logger.info(f'Application logger configured at {APP_LOG_LEVEL} level')
    logger.info('Lab 9 running')
    logger.info('Calling Lab2')
    Lab2.run_lab()

    logger.info('Calling Lab5')
    Lab5.run_lab()
    logger.info('Finished running lab 9')
