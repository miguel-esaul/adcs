# TC4002.1 | ADCS Lab 9. #

This project was created as an assignment for the postgraduate-level course *TC4002.1 Software Analysis, Design and 
Construction* taught at Tecnológico de Monterrey Campus Guadalajara by Ph.D. Gerardo Padilla Zárate.

### Prerequisites ###

* Python 3.6 or above
* `Requirements.txt` file list all Python libraries this project depends on, install them using:

```
pip install -r requirements.txt
```


### Description ###

Original instructions and requirements for this Lab can be found in 
[Instructions](https://bitbucket.org/miguel-esaul/adcs/src/master/Lab9/Lab%20Instructions/).

#### Goals ####

* This exercise provides the experience to implement different logging configurations for your application.


#### Requirements ####

* You are requested to use the python logging module

1. Implement the practice in python
1. Select Lab2.2 Programming assignment 15, and Lab 5.1 Programming assignment 25 and 26
1. Add logs for two programs
    1. Add logs for INFO messages
    1. Add logs for ERROR messages
1. Configure to send the log information to 
    1. the console
    1. to a file
1. Create this new lab as a new program in your repo

### Results ###

* A parent-child structure was used for loggers.
    * Each module has its own named logger, \__main\__ variable is used for this purpose.
    * Additional loggers are created for each custom class. These will be children of the module's logger
    * Testing classes do not have their own logger and use module's logger.
* Parent logger is configured in Lab9_main.py.
    * Two handlers are added to this logger. Specific formatters are linked to each handler.
        * A StreamHandler logger to emit log messages to console.
        * A FileHandler logger to save messages into a .log file.
    * Logger level is configured using module function configure_logger(), default value is INFO.
* LogRecords from Lab2 are pure INFO level records since there are not try exception blocks implemented.
* LogRecords from Lab5 are both INFO and ERROR level records. Some of them include exc_info=TRUE for demonstration 
purposes.
* Unit tests previously developed for both Labs (Lab2 and Lab5) are run to prove logger functionality.
* Lab2 includes console output describing each part of the Programming assignment 15, so this be displayed on console 
along with the logs.

* * *

### Authors ###

* **Miguel Avina** - *Initial Work* - [Bitbucket](https://bitbucket.org/miguel-esaul), 
[Github](https://github.com/miguelAvina1).

### License ###

The MIT License (MIT)

Copyright (c) 2020 Miguel Aviña Pantoja

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
