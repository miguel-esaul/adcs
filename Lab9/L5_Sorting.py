import csv
import logging
import CustomExceptions as Ex
import Lab5_utils as Utils

logger = logging.getLogger(f'__main__.Lab5.{__name__}')   # Creating child logger


def merge_sorted(list1, list2):
    logger.info(f'Executing subroutine for merge sort')
    i = 0
    while len(list2) > 0 and i < len(list1):
        if list2[0] < list1[i]:
            n = list2.pop(0)
            list1.insert(i, n)
        i += 1
    list1.extend(list2)
    return list1


def do_merge_sort(data):
    logger.info(f'Executing merge sort algorithm with {len(data)} data values')
    if len(data) == 0:
        raise Ex.EmptyFile
    elif len(data) == 1:
        return data
    else:
        if len(data) % 2:
            half = len(data) // 2 + 1
        else:
            half = len(data) // 2

        left = data[:half]  # 0 to half non inclusive
        right = data[half:]

        left = do_merge_sort(left)
        right = do_merge_sort(right)

    return merge_sorted(left, right)


class SortingCSVData:
    def __init__(self):
        """ Initialization method. Set a default output file name in case the user doesn't provide one. """
        self.logger = logging.getLogger(f'__main__.Lab5.{__name__}.SortingCSVData')   # Creating child logger
        self.logger.info('Instantiating SortingCVSData class')
        self.input_file_path = ''
        self.output_file_path = 'Merge Sort Output.csv'
        self.correctInputPath = False

    def set_input_data(self, file_path_name):
        """
        This function sets the input file name to the object.
        It also asserts parameters and raise custom exceptions for situations where the parameter is incorrect.
        :param file_path_name: Input file name
        :return: None
        """
        self.logger.info(f'Setting input data filename with "{file_path_name}"')
        if type(file_path_name) != str:
            self.logger.error(f'Incorrect filename. String argument expected, given: {type(file_path_name)}: '
                                  f'{file_path_name}')
            raise Ex.IncorrectType     # Custom exception
        else:
            self.logger.info(f'Attempting to open file: "{file_path_name}"')
            try:
                f = open(file_path_name, 'r')
                self.logger.info(f'"{file_path_name}" added to input_file_path attribute')
                self.input_file_path = file_path_name
                self.correctInputPath = True
                f.close()
            except FileNotFoundError:
                self.logger.error(f'"{file_path_name}" not found in current working directory')
                raise Ex.CVSFileNotFound

    def set_output_data(self, file_path_name):
        """
        This function sets the output file name of the file that will stored the sorted values.
        It also asserts parameters and raise custom exceptions for situations where the parameter is incorrect.
        :param file_path_name: Output file name
        :return: None
        """
        if type(file_path_name) != str:
            self.logger.error(f'Incorrect filename. String argument expected, given: {type(file_path_name)}: '
                              f'{file_path_name}')
            raise Ex.IncorrectType  # Custom exception
        else:
            self.logger.info(f'Attempting to open file: "{file_path_name}" for writing')
            try:
                f = open(file_path_name, 'w')
                self.output_file_path = file_path_name
                f.close()
            except PermissionError:
                self.logger.exception(f'File "{file_path_name}" is a read-only file. Permission to write was denied')
                raise Ex.InvalidFile
            except FileNotFoundError:                       # Although built in exception is specific
                self.logger.exception(f'There was an unexpected error')
                raise Ex.Error                              # for demonstration purposes.

    def execute_merge_sort(self):
        self.logger.info('Executing merge sort method')
        if self.correctInputPath:
            with open(self.input_file_path, 'r') as csv_file:
                reader = csv.reader(csv_file, delimiter=',')
                try:
                    all_data = [int(j) for i in reader for j in i]
                except ValueError:
                    self.logger.warning('Provided values are not type "int", casting them as floats')
                    all_data = [float(j) for i in reader for j in i]
            sorted_data = do_merge_sort(all_data)
            self.logger.info(f'Saving sorted data to {self.output_file_path}')
            Utils.save_data_to_csv(sorted_data, self.output_file_path)
        else:
            self.logger.exception('Input file is invalid or is not specified')
            raise Ex.InputFileNotSpecified
